using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class EndingUIManager : MonoBehaviour
{
    public TMP_Text totalScoreTxt;

    private GameManager gm;

    private void Start()
    {
        gm = GameManager.GetInstance();

        totalScoreTxt.text = gm.totalScore.ToString();
    }

    public void GoToTitle()
    {
        SceneManager.LoadScene("Title");
    }
}
