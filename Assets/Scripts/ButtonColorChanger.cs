using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class ButtonColorChanger : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public TMP_Text buttonTxt;

    public void OnPointerEnter(PointerEventData eventData)
    {
        buttonTxt.color = Color.white;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        buttonTxt.color = Color.black;
    }
}
