using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class GameManager : MonoBehaviour
{
    public static int LastStage = 15;
    public static int ScreenWidth = 1440;
    public static int ScreenHeight = 1080;

    public StageData stageData;
     
    public int currentStage;
    public int totalScore;

    public static GameManager instance;
    public AudioMixer mainAudioMixer;

    private bool isMusicOn;
    private bool isAudioOn;

    public GameManager()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public static GameManager GetInstance()
    {
        if (instance == null)
        {
            instance = new GameManager();
        }

        return instance;
    }

    private void Start()
    {
        GameObject[] objects = GameObject.FindGameObjectsWithTag("GameController");

        if (objects.Length > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }

        isMusicOn = PlayerPrefs.GetInt("MusicOff") == 0 ? true : false;
        isAudioOn = PlayerPrefs.GetInt("AudioOff") == 0 ? true : false;

        if (!isMusicOn)
        {
            MusicOff();
        }

        if (!isAudioOn)
        {
            AudioOff();
        }

        Screen.SetResolution(ScreenWidth, ScreenHeight, true);
    }

    public bool IsMusicOn()
    {
        return isMusicOn;
    }

    public void MusicOn()
    {
        isMusicOn = true;

        PlayerPrefs.SetInt("MusicOff", 0);
        PlayerPrefs.Save();

        mainAudioMixer.SetFloat("BGMVolume", 0);
    }    

    public void MusicOff()
    {
        isMusicOn = false;

        PlayerPrefs.SetInt("MusicOff", 1);
        PlayerPrefs.Save();

        mainAudioMixer.SetFloat("BGMVolume", -80);
    }

    public bool IsAudioOn()
    {
        return isAudioOn;
    }

    public void AudioOn()
    {
        isAudioOn = true;

        PlayerPrefs.SetInt("AudioOff", 0);
        PlayerPrefs.Save();

        mainAudioMixer.SetFloat("SFXVolume", 0);
    }

    public void AudioOff()
    {
        isAudioOn = false;

        PlayerPrefs.SetInt("AudioOff", 1);
        PlayerPrefs.Save();

        mainAudioMixer.SetFloat("SFXVolume", -80);
    }

    public void ResetData()
    {
        stageData = new StageData();
        currentStage = 0;
        totalScore = 0;
    }
}
