using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class ResultUIManager : MonoBehaviour
{
    public TMP_Text completeTxt;
    public TMP_Text scoreThisLevelTxt;
    public TMP_Text totalScoreTxt;
    public TMP_Text timeBonusTxt;
    public TMP_Text timeTakenTxt;
    public TMP_Text previousLevelsTxt;
    public TMP_Text levelBonusTxt;

    private int currentStage;
    private int reachedStage;
    private int levelBonus;
    private int maxTimeBonus;
    private float timeCount;
    private int previousLevels;

    private int scoreThisLevel;
    private int totalScore;
    private int timeBonus;

    private GameManager gm;

    private void Start()
    {
        gm = GameManager.GetInstance();

        LoadData();
        SetData();
        PrintInfo();
    }

    private void LoadData()
    {
        currentStage = gm.currentStage;
        reachedStage = PlayerPrefs.GetInt("ReachedStage");
        levelBonus = gm.stageData.levelBonus;
        maxTimeBonus = gm.stageData.maxTimeBonus;
        timeCount = gm.stageData.timeCount;
        previousLevels = gm.totalScore;
    }

    private void SetData()
    {
        timeBonus = maxTimeBonus - Convert.ToInt32(Math.Floor(timeCount));

        // 시간 보너스는 음수로 내려갈 수 없음
        if (timeBonus < 0)
        {
            timeBonus = 0;
        }

        scoreThisLevel = levelBonus + timeBonus;
        totalScore = previousLevels + scoreThisLevel;

        // 최종 점수 및 스테이지 갱신
        gm.totalScore = totalScore;
        PlayerPrefs.SetInt("ReachedStage", currentStage > reachedStage ? currentStage : reachedStage);
        PlayerPrefs.Save();
    }

    private void PrintInfo()
    {
        TimeSpan t = TimeSpan.FromSeconds(timeCount);

        completeTxt.text = $"Level {currentStage} Complete";

        scoreThisLevelTxt.text = scoreThisLevel.ToString();
        totalScoreTxt.text = totalScore.ToString();
        timeBonusTxt.text = timeBonus.ToString();
        timeTakenTxt.text = string.Format("{0:D2}:{1:D2}", t.Minutes, t.Seconds);
        previousLevelsTxt.text = previousLevels.ToString();
        levelBonusTxt.text = levelBonus.ToString();
    }

    public void GoToNextStage()
    {
        if (currentStage >= GameManager.LastStage)
        {
            SceneManager.LoadScene("Ending");
        }
        else
        {
            // 다음 스테이지 번호 저장 및 스테이지 정보 초기화
            gm.currentStage = currentStage + 1;
            gm.stageData = new StageData();

            SceneManager.LoadScene("Stage");
        }
    }

    public void GoToTitle()
    {
        SceneManager.LoadScene("Title");
    }
}
