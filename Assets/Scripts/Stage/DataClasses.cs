using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageData
{
    public int levelBonus;
    public int maxTimeBonus;
    public float timeCount;
    public Dictionary<int, ItemControllData> moveInfo;

    public StageData()
    {
        levelBonus = 0;
        maxTimeBonus = 0;
        timeCount = 0;
        moveInfo = new Dictionary<int, ItemControllData>();
    }
}

public class ItemControllData
{
    public int ID;
    public int Inventory;
    public Vector2Int SlotPos;
    public bool IsDraggable;
    public int Clicked;
}

public class ItemInfoData
{
    public string Name;
    public string Description;
}