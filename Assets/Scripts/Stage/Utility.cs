using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utility : MonoBehaviour
{
    // 레이캐스트로 체크한 오브젝트들 중 특정 타입의 오브젝트 반환
    public static T GetRaycastHitInfo<T>(RaycastHit2D[] hits)
    {
        T result = default(T);
        string typeName = typeof(T).ToString();

        foreach (RaycastHit2D hit in hits)
        {
            if (hit.collider.CompareTag(typeName))
            {
                result = hit.collider.GetComponent<T>();
                break;
            }
        }

        return result;
    }

    // 아이템이 특정 인벤토리에 속하는지 체크
    public static bool CheckItemBelongs(Item item, ItemSlotGrid slotGrid)
    {
        return GetParentItemSlotGrid(item) == slotGrid ? true : false;
    }

    // 아이템 슬롯이 특정 인벤토리에 속하는지 체크
    public static bool CheckItemBelongs(ItemSlot itemSlot, ItemSlotGrid slotGrid)
    {
        return GetParentItemSlotGrid(itemSlot) == slotGrid ? true : false;
    }

    // 아이템이 위치한 슬롯을 반환
    public static ItemSlot GetParentItemSlot(Item item)
    {
        return item.transform.parent.GetComponent<ItemSlot>();
    }

    // 아이템이 위치한 인벤토리를 반환
    public static ItemSlotGrid GetParentItemSlotGrid(Item item)
    {
        ItemSlot slot = GetParentItemSlot(item);

        if (slot != null)
        {
            return slot.transform.parent.GetComponent<ItemSlotGrid>();
        }
        else
        {
            return null;
        }
    }

    // 아이템 슬롯이 위치한 인벤토리를 반환
    public static ItemSlotGrid GetParentItemSlotGrid(ItemSlot itemSlot)
    {
        return itemSlot.transform.parent.GetComponent<ItemSlotGrid>();
    }

    // 아이템의 타입을 반환
    public static ItemType GetItemType(Item item)
    {
        ItemType type;
        string typeString = item.GetType().ToString();

        // 전선, 거울의 경우 별도 처리
        if (item is ConnectionWire)
        {
            if (item.name.Contains("Vertical"))
            {
                typeString = "VerticalWire";
            }
            else if (item.name.Contains("Horizontal"))
            {
                typeString = "HorizontalWire";
            }
        }
        else if (item is Mirror)
        {
            if (item.name.Contains("Upper"))
            {
                typeString = "UpperMirror";
            }
            else if (item.name.Contains("Lower"))
            {
                typeString = "LowerMirror";
            }
        }

        if (!Enum.TryParse(typeString, out type))
        {
            Debug.Log("Enum Parse Error");
        }

        return type;
    }
}
