using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MusicBtn : MonoBehaviour
{
    public TMP_Text buttonTxt;

    private bool isMusicOn;

    private void Start()
    {
        GameManager gm = GameManager.GetInstance();
        isMusicOn = gm.IsMusicOn();

        SetButtonTxt();
    }

    public void ToggleMusic()
    {
        GameManager gm = GameManager.GetInstance();

        if (isMusicOn)
        {
            isMusicOn = false;
            gm.MusicOff();
        }
        else
        {
            isMusicOn = true;
            gm.MusicOn();
        }

        SetButtonTxt();
    }

    private void SetButtonTxt()
    {
        if (isMusicOn)
        {
            buttonTxt.text = "Music\nON";
        }
        else
        {
            buttonTxt.text = "Music\nOFF";
        }
    }
}
