using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AudioBtn : MonoBehaviour
{
    public TMP_Text buttonTxt;

    private bool isAudioOn;

    private void Start()
    {
        GameManager gm = GameManager.GetInstance();
        isAudioOn = gm.IsAudioOn();

        SetButtonTxt();
    }

    public void ToggleAudio()
    {
        GameManager gm = GameManager.GetInstance();

        if (isAudioOn)
        {
            isAudioOn = false;
            gm.AudioOff();
        }
        else
        {
            isAudioOn = true;
            gm.AudioOn();
        }

        SetButtonTxt();
    }

    private void SetButtonTxt()
    {
        if (isAudioOn)
        {
            buttonTxt.text = "Audio\nON";
        }
        else
        {
            buttonTxt.text = "Audio\nOFF";
        }
    }
}
