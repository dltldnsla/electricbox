using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class PlayManager : MonoBehaviour
{
    public bool isSimulating;
    public ItemSlotGrid gameZone;
    public ItemSlotGrid inventory;
    public StageDB stageDB;
    public TMP_Text stageNumTxt;

    private float timeCount;
    private List<Item> items;
    private GameManager gm;

    private void Start()
    {
        gm = GameManager.GetInstance();
        isSimulating = false;

        stageNumTxt.text = gm.currentStage.ToString();

        LoadStage();
        ApplyMoveInfo();
    }

    private void Update()
    {
        timeCount += Time.deltaTime;
    }

    // 시뮬레이션 시작
    public void StartSimulate()
    {
        isSimulating = true;
        SaveMoveInfo();

        List<Item> checkItems = FindItemsFromSlotGrid(gameZone);

        // 각 아이템의 동작 시작
        foreach (Item item in checkItems)
        {
            item.StartFunctions();
        }
    }

    // 시뮬레이션 종료
    public void StopSimulate()
    {
        isSimulating = false;

        // 현재까지 소요된 시간을 GameManager에 저장
        gm.stageData.timeCount = timeCount;

        SceneManager.LoadScene("Stage");
    }

    // 지정된 슬롯 그리드에서 아이템을 검색
    private List<Item> FindItemsFromSlotGrid(ItemSlotGrid slotGrid)
    {
        List<Item> checkItems = new List<Item>();

        // 전선 검색
        GameObject[] connectionWires = GameObject.FindGameObjectsWithTag("ConnectionWire");
        foreach (GameObject item in connectionWires)
        {
            ConnectionWire wire = item.GetComponent<ConnectionWire>();
            ItemSlot slot = Utility.GetParentItemSlot(wire);

            if (slot.transform.parent == slotGrid.transform)
            {
                checkItems.Add(wire);
            }
        }

        // 아이템 검색
        GameObject[] interactiveItems = GameObject.FindGameObjectsWithTag("InteractiveItem");
        foreach (GameObject item in interactiveItems)
        {
            InteractiveItem interactiveItem = item.GetComponent<InteractiveItem>();
            ItemSlot slot = Utility.GetParentItemSlot(interactiveItem);

            if (slot != null && slot.transform.parent == slotGrid.transform)
            {
                checkItems.Add(interactiveItem);
            }
        }

        return checkItems;
    }

    public void StageClear()
    {
        // 소요 시간 저장
        gm.stageData.timeCount = timeCount;

        StartCoroutine(WaitForNextStage());
    }

    IEnumerator WaitForNextStage()
    {
        yield return new WaitForSeconds(1);

        SceneManager.LoadScene("Result");
    }

    // 파일로부터 스테이지의 정보를 로드하여 적용
    private void LoadStage()
    {
        items = new List<Item>();

        int currentStage = gm.currentStage;
        timeCount = gm.stageData.timeCount;

        // 스테이지 정보를 불러와 GameManager에 저장
        StageData stageInfo = stageDB.LoadStageInfoDB(currentStage);
        gm.stageData.levelBonus = stageInfo.levelBonus;
        gm.stageData.maxTimeBonus = stageInfo.maxTimeBonus;

        // 아이템 정보를 불러온 후, 이를 기반으로 아이템 생성 및 배치
        List<ItemControllData> stageItemInfo = stageDB.LoadStageItemDB(currentStage);
        Dictionary<int, ItemInfoData> itemInfo = stageDB.LoadItemInfoDB();

        foreach (ItemControllData itemControll in stageItemInfo)
        {
            Item item = ItemFactory.CreateItem((ItemType)itemControll.ID);
            item.isDraggable = itemControll.IsDraggable;

            if (item is InteractiveItem interactiveItem)
            {
                for (int i = 0; i < itemControll.Clicked; i++)
                {
                    interactiveItem.OnMouseClick();
                }
            }

            if (itemControll.Inventory == 0)
            {
                gameZone.PlaceItem(itemControll.SlotPos, item);
            }
            else if (itemControll.Inventory == 1)
            {
                inventory.PlaceItem(itemControll.SlotPos, item);
            }

            item.itemName = itemInfo[itemControll.ID].Name;
            item.itemDescription = itemInfo[itemControll.ID].Description;

            items.Add(item);
        }
    }

    // 플레이어가 오브젝트를 이동/조작한 내역이 있을 경우, 이를 반영함
    private void ApplyMoveInfo()
    {
        Dictionary<int, ItemControllData> moveInfo = gm.stageData.moveInfo;

        foreach (KeyValuePair<int, ItemControllData> info in moveInfo)
        {
            int index = info.Key;
            ItemControllData itemData = info.Value;

            Item item = items[index];
            ItemSlot currentSlot = Utility.GetParentItemSlot(item);
            ItemSlotGrid currentSlotGrid = Utility.GetParentItemSlotGrid(currentSlot);

            // 방향 전환 처리
            if (item is InteractiveItem interactiveItem)
            {
                for (int i = 0; i < itemData.Clicked; i++)
                {
                    interactiveItem.OnMouseClick();
                }
            }

            // 지정된 위치로 아이템 이동
            currentSlotGrid.RemoveItem(currentSlot.slotCoordinate, item);

            if (itemData.Inventory == 0)
            {
                gameZone.PlaceItem(itemData.SlotPos, item);
            }
            else if (itemData.Inventory == 1)
            {
                inventory.PlaceItem(itemData.SlotPos, item);
            }
        }
    }

    private void SaveMoveInfo()
    {
        gm.stageData.moveInfo.Clear();

        for (int i = 0; i < items.Count; i++)
        {
            if (items[i] is InteractiveItem)
            {
                gm.stageData.moveInfo.Add(i, CreateMoveInfo(items[i]));
            }
        }
    }  

    // 아이템 조작 정보를 ItemData로 변환
    private ItemControllData CreateMoveInfo(Item item)
    {
        int inventoryNum = int.MaxValue;

        if (Utility.CheckItemBelongs(item, gameZone))
        {
            inventoryNum = 0;
        }
        else if (Utility.CheckItemBelongs(item, inventory))
        {
            inventoryNum = 1;
        }

        ItemControllData data = new ItemControllData();
        data.SlotPos = Utility.GetParentItemSlot(item).slotCoordinate;
        data.Inventory = inventoryNum;
        data.Clicked = item.transform.localScale.x > 0 ? 0 : 1;

        return data;
    }

    public void GoToTitle()
    {
        SceneManager.LoadScene("Title");
    }

    public void GoToLevels()
    {
        SceneManager.LoadScene("Levels");
    }



    // *** 테스트용 코드 시작 ***
    // 스테이지 데이터를 만드는 데 사용함

    //private void SaveStageInfo(int stageNum)
    //{
    //    List<Item> gameZoneItems = FindItemsFromSlotGrid(gameZone);
    //    List<Item> inventoryItems = FindItemsFromSlotGrid(inventory);

    //    List<ItemControllData> itemList = new List<ItemControllData>();

    //    foreach (Item item in gameZoneItems)
    //    {
    //        itemList.Add(CreateItemData(item, 0));
    //    }

    //    foreach (Item item in inventoryItems)
    //    {
    //        itemList.Add(CreateItemData(item, 1));
    //    }

    //    stageDB.SaveItemDB(stageNum, itemList);
    //}

    //private ItemControllData CreateItemData(Item item, int zoneNum)
    //{
    //    string itemName = item.name.Replace("(Clone)", "");
    //    ItemType type = (ItemType)Enum.Parse(typeof(ItemType), itemName);

    //    ItemControllData data = new ItemControllData();
    //    data.ID = (int)type;
    //    data.Inventory = zoneNum;
    //    data.SlotPos = Utility.GetParentItemSlot(item).slotCoordinate;
    //    data.IsDraggable = item.isDraggable;
    //    data.Clicked = item.transform.localScale.x > 0 ? 0 : 1;

    //    return data;
    //}

    //// 입력 값에 따라 아이템을 생성하여 지정 위치에 배치
    //private void SetItem(ItemType type, ItemSlotGrid inventory, int posX, int posY, bool isDraggable, int clicked)
    //{
    //    Item item = ItemFactory.CreateItem(type);
    //    item.isDraggable = isDraggable;

    //    if (item is InteractiveItem interactiveItem)
    //    {
    //        for (int i = 0; i < clicked; i++)
    //        {
    //            interactiveItem.OnMouseClick();
    //        }
    //    }

    //    inventory.PlaceItem(new Vector2Int(posX, posY), item);
    //}

    // *** 테스트용 코드 종료 ***
}
