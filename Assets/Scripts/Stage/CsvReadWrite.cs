using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CsvReadWrite : MonoBehaviour
{
    public static string[][] LoadTextFile(TextAsset textFile)
    {
        if (textFile != null)
        {
            string[] row = textFile.text.Split('\n');
            List<string> rowData = row.ToList();
            rowData = rowData.Where(s => !string.IsNullOrWhiteSpace(s)).ToList();

            string[][] grid = new string[rowData.Count][];
            for (int i = 0; i < rowData.Count; i++)
            {
                grid[i] = rowData[i].Split(',');
            }
            return grid.ToArray();
        }
        else
        {
            Debug.Log("File does not exist.");
            return null;
        }
    }

    //public static void WriteToFile(string[][] strList, TextAsset textFile)
    //{
    //    if (textFile != null)
    //    {
    //        string[] lines = new string[strList.Length];
    //        for (int i = 0; i < strList.Length; i++)
    //        {
    //            lines[i] = string.Join(",", strList[i]);
    //        }
    //        File.WriteAllLines(AssetDatabase.GetAssetPath(textFile), lines);
    //    }
    //    else
    //    {
    //        Debug.Log("File does not exist.");
    //    }
    //}
}
