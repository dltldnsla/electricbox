using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using TMPro;

public class ItemController : MonoBehaviour
{
    public TMP_Text itemNameTxt;
    public TMP_Text itemDescriptionTxt;

    public AudioSource clickSound;

    private Item targetItem;    // 마우스가 가리키고 있는 아이템
    private Item selectedItem;  // 마우스로 클릭하여 선택한 아이템
    private bool isDraggable;

    private ItemSlot previousItemSlot;
    private ItemSlotGrid previousSlotGrid;
    private ItemSlot currentItemSlot;
    private ItemSlotGrid currentSlotGrid;

    private RaycastHit2D[] hits;
    private PlayManager pm;

    private void Awake()
    {
        targetItem = null;
        selectedItem = null;
        isDraggable = false;

        itemNameTxt.text = "";
        itemDescriptionTxt.text = "";

        pm = GameObject.Find("PlayManager").GetComponent<PlayManager>();
    }

    private void Update()
    {
        // 레이캐스트로 아이템 및 아이템 슬롯 체크
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        hits = Physics2D.RaycastAll(ray.origin, ray.direction);

        // 현재 마우스가 가리키고 있는 아이템의 설명을 출력
        PrintItemDescription();

        // 마우스 입력에 따른 아이템 드래그&드롭 처리
        if (Input.GetMouseButton(0) || Input.GetMouseButtonDown(0) || Input.GetMouseButtonUp(0))
        {
            if(Input.GetMouseButtonDown(0))
            {
                OnStartDrag();
            }
            else if(Input.GetMouseButton(0))
            {
                OnDrag();
            }
            else if(Input.GetMouseButtonUp(0))
            {
                OnDragEnd();
            }
        }
    }

    // 드래그 시작
    private void OnStartDrag()
    {
        if (selectedItem == null)
        {
            // 시뮬레이션 중에는 PowerSupply만 조작할 수 있음
            if (pm.isSimulating)
            {
                selectedItem = Utility.GetRaycastHitInfo<InteractiveItem>(hits);

                if (selectedItem != null)
                {
                    if (!(selectedItem is PowerSupply))
                    {
                        selectedItem = null;
                    }
                }
            }
            else
            {
                // 마우스 위치에 아이템이 있는지 체크
                selectedItem = Utility.GetRaycastHitInfo<InteractiveItem>(hits);

                // 아이템이 없다면, 전선이 있는지 체크
                if (selectedItem == null)
                {
                    selectedItem = Utility.GetRaycastHitInfo<ConnectionWire>(hits);
                }

                if (selectedItem != null)
                {
                    isDraggable = selectedItem.isDraggable;

                    // 이동 가능한 아이템일 경우, 아이템의 투명도를 낮춤
                    if (isDraggable)
                    {
                        selectedItem.SetTransparent(true);
                    }
                }
            }
        }
    }

    // 드래그 진행 중
    private void OnDrag()
    {
        if(selectedItem != null && isDraggable)
        {
            // 드래그 처리
            Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, -Camera.main.transform.position.z);
            selectedItem.transform.position = Camera.main.ScreenToWorldPoint(mousePosition) + selectedItem.GetFirstSlotPos();

            currentItemSlot = Utility.GetRaycastHitInfo<ItemSlot>(hits);

            // 현재 마우스가 기존에 가리키던 슬롯을 벗어났을 경우, 기존 슬롯 색깔 초기화
            if (previousItemSlot != null && previousItemSlot != currentItemSlot)
            {
                previousSlotGrid = Utility.GetParentItemSlotGrid(previousItemSlot);
                previousSlotGrid.SetSlotColor(previousItemSlot.slotCoordinate, selectedItem, ItemSlot.Color.Default);
            }

            previousItemSlot = currentItemSlot;

            // 현재 마우스가 가리키는 슬롯에 아이템을 드롭할 수 있는지 체크하고, 그에 따른 슬롯 색 변경
            if(currentItemSlot != null)
            {
                currentSlotGrid = Utility.GetParentItemSlotGrid(currentItemSlot);
                ItemSlot.Color slotColor = ItemSlot.Color.Red;

                if(currentSlotGrid.CheckItemDroppable(currentItemSlot.slotCoordinate, selectedItem))
                {
                    slotColor = ItemSlot.Color.Green;
                }

                currentSlotGrid.SetSlotColor(currentItemSlot.slotCoordinate, selectedItem, slotColor);
            }
        }
    }

    // 드래그 종료
    private void OnDragEnd()
    {
        bool itemMoved = false;

        if(selectedItem != null && isDraggable)
        {
            // 마우스 위치에 아이템 슬롯이 있는지 체크
            currentItemSlot = Utility.GetRaycastHitInfo<ItemSlot>(hits);

            if (currentItemSlot != null)
            {
                // 해당 슬롯에 아이템을 이동 가능한 지 체크
                currentSlotGrid = Utility.GetParentItemSlotGrid(currentItemSlot);

                if (currentSlotGrid.CheckItemDroppable(currentItemSlot.slotCoordinate, selectedItem))
                {
                    // 드래그 시작 위치와 종료 위치가 다를 경우, 아이템을 이동
                    if(currentItemSlot.transform != selectedItem.transform.parent)
                    {
                        TransferItemInfo();
                        itemMoved = true;
                    }
                }

                // 현재 슬롯의 색 초기화
                currentSlotGrid.SetSlotColor(currentItemSlot.slotCoordinate, selectedItem, ItemSlot.Color.Default);
            }

            // 아이템의 위치를 부모 오브젝트(아이템 슬롯) 기준으로 이동하고, 투명도를 원래대로 되돌림
            selectedItem.transform.position = selectedItem.transform.parent.position + selectedItem.GetFirstSlotPos();
            selectedItem.SetTransparent(false);
        }

        // 아이템이 이동하지 않았을 경우, 해당 아이템의 클릭 상호작용을 수행
        if (!itemMoved && selectedItem is InteractiveItem interactiveItem)
        {
            interactiveItem.OnMouseClick();
        }

        // 아이템을 조작했을 경우, 효과음 발생
        if (selectedItem != null)
        {
            clickSound.Play();
        }

        selectedItem = null;
    }

    // 아이템 정보를 기존 슬롯에서 현재 슬롯으로 이동
    private void TransferItemInfo()
    {
        // 아이템이 있던 기존 슬롯의 아이템 정보 제거
        previousItemSlot = Utility.GetParentItemSlot(selectedItem);
        previousSlotGrid = Utility.GetParentItemSlotGrid(previousItemSlot);

        previousSlotGrid.RemoveItem(previousItemSlot.slotCoordinate, selectedItem);

        // 아이템을 새로운 슬롯의 자식으로 설정하고, 아이템 드롭 영역의 슬롯에 아이템 정보 추가
        selectedItem.transform.SetParent(currentItemSlot.transform);
        currentSlotGrid.SetItem(currentItemSlot.slotCoordinate, selectedItem);
    }

    public void PrintItemDescription()
    {
        // 마우스 위치에 아이템이 있는지 체크
        Item currentItem = Utility.GetRaycastHitInfo<InteractiveItem>(hits);

        // 아이템이 없다면, 전선이 있는지 체크
        if (currentItem == null)
        {
            currentItem = Utility.GetRaycastHitInfo<ConnectionWire>(hits);
        }

        if (currentItem != targetItem)
        {
            targetItem = currentItem;

            if (targetItem != null)
            {
                itemNameTxt.text = targetItem.itemName;
                itemDescriptionTxt.text = targetItem.itemDescription;
            }
            else
            {
                itemNameTxt.text = "";
                itemDescriptionTxt.text = "";
            }
        }
    }
}
