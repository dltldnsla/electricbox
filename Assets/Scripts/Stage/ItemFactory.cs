using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemType
{
    VerticalWire,
    HorizontalWire,
    WaterDispenser,
    PowerSupply,
    Target,
    WaterTurbine,
    IPSBattery,
    Fan,
    ElectroMagnet,
    SteamDetector,
    LightBulb,
    ChargerLight,
    UpperMirror,
    LowerMirror,
    SolarPanel,
    ElectricKettle,
    TransporterBot,
    Windmill,
    BallDropper,
    Refrigerator,
    LaserDetector,
    LaserDevice,
    Block,
}

public class ItemFactory : MonoBehaviour
{
    public static Item CreateItem(ItemType type)
    {
        string fileName = type.ToString();
        string path = "Prefabs\\Items\\";

        GameObject prefab = Resources.Load(path + fileName) as GameObject;
        GameObject itemObj = Instantiate(prefab, prefab.transform.position, Quaternion.identity);

        return itemObj.GetComponent<Item>();
    }
}
