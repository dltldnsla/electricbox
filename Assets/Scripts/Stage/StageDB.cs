using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageDB : MonoBehaviour
{
    public TextAsset stageInfoFile;
    public TextAsset stageItemFile;
    public TextAsset itemInfoFile;
    //public TextAsset writeFile;

    public StageData LoadStageInfoDB(int stageNum)
    {
        StageData data = new StageData();
        string[][] grid = CsvReadWrite.LoadTextFile(stageInfoFile);
        for (int i = 1; i < grid.Length; i++)
        {
            int stNum = int.Parse(grid[i][0]);

            if (stNum == stageNum)
            {
                data.levelBonus = int.Parse(grid[i][1]);
                data.maxTimeBonus = int.Parse(grid[i][2]);

                break;
            }
        }

        return data;
    }

    public List<ItemControllData> LoadStageItemDB(int stageNum)
    {
        List<ItemControllData> data = new List<ItemControllData>();
        string[][] grid = CsvReadWrite.LoadTextFile(stageItemFile);
        for (int i = 1; i < grid.Length; i++)
        {
            int stNum = int.Parse(grid[i][0]);

            if (stNum == stageNum)
            {
                ItemControllData row = new ItemControllData();
                row.ID = int.Parse(grid[i][1]);
                row.Inventory = int.Parse(grid[i][2]);
                row.SlotPos = new Vector2Int(int.Parse(grid[i][3]), int.Parse(grid[i][4]));
                row.IsDraggable = int.Parse(grid[i][5]) == 0 ? false : true;
                row.Clicked = int.Parse(grid[i][6]);

                data.Add(row);
            }
        }

        return data;
    }

    public Dictionary<int, ItemInfoData> LoadItemInfoDB()
    {
        Dictionary<int, ItemInfoData> data = new Dictionary<int, ItemInfoData>();
        string[][] grid = CsvReadWrite.LoadTextFile(itemInfoFile);
        for (int i = 1; i < grid.Length; i++)
        {
            int id = int.Parse(grid[i][0]);
            
            // 설명문 관련 처리
            string description = string.Join(',', grid[i].Skip(2).Take(grid[i].Length - 2));
            description = description.Replace("\"\"", "\"");

            if (description[0] == '\"')
            {
                description = description.Substring(1, description.Length - 3);
            }

            // 데이터 클래스 생성
            ItemInfoData row = new ItemInfoData();
            row.Name = grid[i][1];
            row.Description = description;

            data.Add(id, row);
        }

        return data;
    }


    // *** 스테이지 데이터 작성용 ***

    //public void SaveItemDB(int stageNum, List<ItemControllData> data)
    //{
    //    string[][] strList = new string[data.Count][];
        
    //    for (int i = 0; i < data.Count; i++)
    //    {
    //        ItemControllData item = data[i];
    //        strList[i] = new string[7];

    //        strList[i][0] = stageNum.ToString();
    //        strList[i][1] = item.ID.ToString();
    //        strList[i][2] = item.Inventory.ToString();
    //        strList[i][3] = item.SlotPos.x.ToString();
    //        strList[i][4] = item.SlotPos.y.ToString();
    //        strList[i][5] = item.IsDraggable ? "1" : "0";
    //        strList[i][6] = item.Clicked.ToString();
    //    }

    //    CsvReadWrite.WriteToFile(strList, writeFile);
    //}
}
