using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectionWire : Item
{
    public PowerGenerator[] powerGenerator;
    public PowerChecker powerChecker;

    private bool powerOn = false;       // 테스트용 변수

    private void Start()
    {
        powerChecker.onChargeStart = OnPowerChargeStart;
        powerChecker.onCharged = OnPowerCharged;
        powerChecker.onDischarged = OnPowerDischarged;
    }

    private void OnPowerChargeStart() { }

    private void OnPowerCharged(Vector2Int powerSourcePos)
    {
        ItemSlot wireSlot = Utility.GetParentItemSlot(this);
        Vector2Int wireSlotPos = wireSlot.slotCoordinate;

        if (powerSourcePos == wireSlotPos)
        {
            powerGenerator[1].GenerateStart();
        }
        else
        {
            powerGenerator[0].GenerateStart();
        }

        powerOn = true;     // 테스트용
    }

    private void OnPowerDischarged()
    {
        foreach (PowerGenerator generator in powerGenerator)
        {
            generator.GenerateStop();
        }

        powerOn = false;    // 테스트용
    }

    public override void StartFunctions()
    {
        powerChecker.StartPowerCheck();
    }

    public override void StopFunctions()
    {
        powerChecker.StopPowerCheck();

        foreach (PowerGenerator generator in powerGenerator)
        {
            generator.GenerateStop();
        }

        powerOn = false;    // 테스트용
    }

    private void Update()
    {
        if (powerOn)
        {
            SpriteRenderer renderer = transform.GetComponent<SpriteRenderer>();
            renderer.color = new Color32(255, 255, 255, 255);
        }
        else
        {
            SpriteRenderer renderer = transform.GetComponent<SpriteRenderer>();
            renderer.color = new Color32(127, 127, 127, 255);
        }
    }
}
