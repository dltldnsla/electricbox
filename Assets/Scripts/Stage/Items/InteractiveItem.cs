using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InteractiveItem : Item
{
    public abstract void OnMouseClick();
}
