using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : Particle
{
    public float speed;

    private Vector3 destination;        // 다음으로 이동할 슬롯의 위치
    private Vector2Int direction;       // 이동 방향(슬롯 좌표 기준)
    private ItemSlot nextSlot;          // 다음으로 이동할 슬롯

    private void Update()
    {
        // 목적지 방향으로 등속 이동
        transform.position = Vector3.MoveTowards(transform.position, destination, speed * Time.deltaTime);

        // 목적지에 도착했을 경우
        if (transform.position == destination)
        {
            // 목적지에 아이템이 있을 경우
            if (nextSlot != null && nextSlot.containedItem != null)
            {
                bool destroyCheck = true;
                InteractiveItem item = nextSlot.containedItem;

                // 아이템이 거울일 경우, 입사각에 따라 반사 또는 충돌 처리
                if (item is Mirror mirror)
                {
                    // 실제로 아이템이 이동하는 방향
                    Vector2 moveDirection = new Vector2(direction.x, -direction.y);

                    if (mirror.ReflectCheck(moveDirection))
                    {
                        Vector2 reflectVector = mirror.GetReflectVector(moveDirection);
                        direction = new Vector2Int(Mathf.RoundToInt(reflectVector.x), Mathf.RoundToInt(-reflectVector.y));
                        SetDestination(nextSlot);

                        destroyCheck = false;
                    }
                }

                if (destroyCheck)
                {
                    // Autodestruct 옵션에 의해, Trail이 사라지면 자동 제거
                    speed = 0;
                }
            }
            // 목적지에 아이템이 없을 경우, 다음 목적지 탐색
            else
            {
                SetDestination(nextSlot);
            }
        }
    }

    // LaserChecker와 충돌 시 처리
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("ItemFunction"))
        {
            if (collision.name == "LaserChecker")
            {
                LaserChecker checker = collision.GetComponent<LaserChecker>();
                checker.Charge();
            }

            // Autodestruct 옵션에 의해, Trail이 사라지면 자동 제거
            speed = 0;
        }
    }

    public void Initiate(ItemSlot itemSlot, Vector2Int dir)
    {
        nextSlot = itemSlot;
        destination = itemSlot.transform.position;
        direction = dir;
    }

    // 현재 슬롯을 기준으로 다음 목적지를 설정
    public void SetDestination(ItemSlot currentSlot)
    {
        // 현재 레이저가 특정 슬롯 위에 있을 경우
        if (currentSlot != null)
        {
            ItemSlotGrid gameZone = Utility.GetParentItemSlotGrid(currentSlot);
            Vector2Int currentCoordinate = currentSlot.slotCoordinate;

            nextSlot = gameZone.GetItemSlot(direction + currentCoordinate);
        }

        // 다음 목적지 설정
        if (nextSlot != null)
        {
            destination = nextSlot.transform.position;
        }
        else
        {
            destination = transform.position + (new Vector3(direction.x, direction.y * (-1), 0) * speed);
        }
    }
}
