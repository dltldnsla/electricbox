using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Water : Particle
{
    public float speed;

    // 아래 방향으로 등속 이동
    private void Update()
    {
        transform.position += Vector3.down * speed * Time.deltaTime;
    }

    // 다른 아이템과 충돌 시 처리
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // 물과 상호작용 없는 아이템과 충돌 시, 물 제거
        if (collision.CompareTag("InteractiveItem"))
        {
            Transform checkerTransform = collision.transform.Find("WaterChecker");

            if (checkerTransform == null)
            {
                Destroy(transform.gameObject);
            }
        }
        // WaterChecker와 충돌 시 처리
        else if (collision.CompareTag("ItemFunction"))
        {
            if (collision.name == "WaterChecker")
            {
                WaterChecker checker = collision.GetComponent<WaterChecker>();
                checker.Charge();
            }

            Destroy(transform.gameObject);
        }
    }
}
