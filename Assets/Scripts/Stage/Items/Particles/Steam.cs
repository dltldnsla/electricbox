using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Steam : Particle
{
    public float speed;

    private Vector3 destination;        // 다음으로 이동할 슬롯의 위치
    private Vector2Int direction;       // 이동 방향
    private ItemSlot nextSlot;          // 다음으로 이동할 슬롯

    private void Update()
    {
        // 목적지 방향으로 등속 이동
        transform.position = Vector3.MoveTowards(transform.position, destination, direction.magnitude * speed * Time.deltaTime);

        // 목적지에 도착했을 경우
        if (transform.position == destination)
        {
            // 목적지에 아이템이 있을 경우, 증기 파괴
            if (nextSlot != null && nextSlot.containedItem != null)
            {
                Destroy(transform.gameObject);
            }
            // 목적지에 아이템이 없을 경우, 다음 목적지 탐색
            else
            {
                SetDestination(nextSlot);
            }
        }
    }

    // SteamChecker와 충돌 시 처리
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("ItemFunction"))
        {
            if (collision.name == "SteamChecker")
            {
                SteamChecker checker = collision.GetComponent<SteamChecker>();
                checker.Charge();
            }

            Destroy(transform.gameObject);
        }
    }

    public void Initiate(ItemSlot itemSlot)
    {
        nextSlot = itemSlot;
        destination = itemSlot.transform.position;
        direction = new Vector2Int(0, -1);
    }

    // 현재 슬롯을 기준으로 다음 목적지를 설정
    private void SetDestination(ItemSlot currentSlot)
    {
        direction = new Vector2Int(0, -1);

        // 현재 증기가 특정 슬롯 위에 있을 경우
        if (currentSlot != null)
        {
            ItemSlotGrid gameZone = Utility.GetParentItemSlotGrid(currentSlot);
            Vector2Int currentCoordinate = currentSlot.slotCoordinate;

            nextSlot = gameZone.GetItemSlot(direction + currentCoordinate);

            // 윗 슬롯이 존재할 경우, 해당 슬롯의 바람을 체크
            if (nextSlot != null)
            {
                if (nextSlot.GetWindDirection().x > 0)
                {
                    direction = new Vector2Int(1, -1);
                    nextSlot = gameZone.GetItemSlot(direction + currentCoordinate);
                    
                }
                else if (nextSlot.GetWindDirection().x < 0)
                {
                    direction = new Vector2Int(-1, -1);
                    nextSlot = gameZone.GetItemSlot(direction + currentCoordinate);
                }
            }
        }

        // 다음 목적지 설정
        if (nextSlot != null)
        {
            destination = nextSlot.transform.position;
        }
        else
        {
            destination = transform.position + (new Vector3(direction.x, direction.y * (-1), 0) * direction.magnitude * speed);
        }
    }
}
