using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// ToDo : 아이템 운송 중 아이템이 다른 아이템과 충돌할 경우, Transporter도 정지하도록 로직 수정
public class ItemTransporter : ItemFunction
{
    public delegate void OnTransportStart();
    public delegate void OnTransportFinished();

    public OnTransportFinished onTransportStart;
    public OnTransportFinished onTransportFinished;

    public float moveSpeed;

    private Vector2Int targetDirection;     // 이동시킬 아이템이 있는 방향
    private InteractiveItem targetItem;     // 이동시킬 아이템
    private InteractiveItem parentItem;     // Transporter를 가지고 있는 부모 아이템
    
    private Vector3 destination;        // 다음으로 이동할 슬롯의 위치
    private Vector2Int moveDirection;   // 이동 방향
    private ItemSlot nextSlot;          // 다음으로 이동할 슬롯

    private void Awake()
    {
        nextSlot = null;
        targetItem = null;
        parentItem = transform.parent.GetComponent<InteractiveItem>();
    }

    private void Update()
    {
        if (nextSlot != null)
        {
            // 목적지 방향으로 등속 이동
            parentItem.transform.position = Vector3.MoveTowards(parentItem.transform.position, destination, moveSpeed * Time.deltaTime);

            // 목적지에 도착했을 경우
            if (parentItem.transform.position == destination)
            {
                // 다음 슬롯이 비어있을 경우, 해당 슬롯으로 경로를 재설정
                if (CheckNextSlot(nextSlot))
                {
                    SetDestination();
                }
                // 다음 슬롯이 비어있지 않을 경우, 이동 종료
                else
                {
                    TransportFinish();
                }
            }
        }
    }

    public void TransportItem(Vector2Int moveDir, Vector2Int targetDir, Type[] availableItems)
    {
        // 현재 정지 상태일 경우
        if (nextSlot == null)
        {
            // 이동 정보 설정
            ItemSlot currentSlot = Utility.GetParentItemSlot(parentItem);
            ItemSlotGrid slotGrid = Utility.GetParentItemSlotGrid(currentSlot);

            targetDirection = targetDir;
            moveDirection = moveDir;
            nextSlot = currentSlot;
            destination = currentSlot.transform.position;

            parentItem.StopFunctions();
            slotGrid.RemoveItem(currentSlot.slotCoordinate, parentItem);

            // 운반할 아이템 설정
            ItemSlot targetItemSlot = slotGrid.GetItemSlot(currentSlot.slotCoordinate + targetDir);

            if(targetItemSlot != null)
            {
                targetItem = targetItemSlot.containedItem;

                if(targetItem != null)
                {
                    // 운반 가능한 아이템일 경우
                    if (CheckItemType(targetItem, availableItems))
                    {
                        // 다음 슬롯으로 이동 가능한 경우에만 아이템을 운송 상태로 변경
                        if (CheckNextSlot(currentSlot))
                        {
                            targetItem.StopFunctions();
                            slotGrid.RemoveItem(targetItemSlot.slotCoordinate, targetItem);

                            targetItem.transform.parent = parentItem.transform;
                        }
                    }
                    else
                    {
                        targetItem = null;
                    }
                }
            }

            // 이동 조건을 만족하면 출발 신호를 보냄
            if (CheckNextSlot(currentSlot))
            {
                onTransportStart();
            }
        }
    }

    // 다음 슬롯으로 이동 가능한지 체크
    public bool CheckNextSlot(ItemSlot slot)
    {
        ItemSlotGrid slotGrid = Utility.GetParentItemSlotGrid(slot);

        Vector2Int checkSlot1Pos = slot.slotCoordinate + moveDirection;
        Vector2Int checkSlot2Pos = slot.slotCoordinate + moveDirection + targetDirection;

        ItemSlot checkSlot1 = slotGrid.GetItemSlot(checkSlot1Pos);
        ItemSlot checkSlot2 = slotGrid.GetItemSlot(checkSlot2Pos);

        // 다음 슬롯 체크
        if (!CheckSlotMoveable(checkSlot1))
        {
            return false;
        }

        // 운송 중인 아이템이 있을 경우, 해당 아이템을 기준으로 다음 슬롯 체크
        if (targetItem != null && !CheckSlotMoveable(checkSlot2))
        {
            return false;
        }

        return true;
    }

    // 대상 슬롯으로 이동 가능한지 체크
    public bool CheckSlotMoveable(ItemSlot slot)
    {
        if (slot == null)
        {
            return false;
        }

        if (slot.containedItem != null)
        {
            return false;
        }

        return true;
    }

    // 다음 이동 경로 설정
    private void SetDestination()
    {
        ItemSlotGrid slotGrid = Utility.GetParentItemSlotGrid(nextSlot);
        ItemSlot currentSlot = nextSlot;

        nextSlot = slotGrid.GetItemSlot(currentSlot.slotCoordinate + moveDirection);
        destination = nextSlot.transform.position;
    }

    // 아이템 운반 종료
    private void TransportFinish()
    {
        ItemSlotGrid slotGrid = Utility.GetParentItemSlotGrid(nextSlot);

        slotGrid.PlaceItem(nextSlot.slotCoordinate, parentItem);
        parentItem.StartFunctions();

        if (targetItem != null)
        {
            slotGrid.PlaceItem(nextSlot.slotCoordinate + targetDirection, targetItem);
            targetItem.StartFunctions();
        }

        targetItem = null;
        nextSlot = null;
        onTransportFinished();
    }

    private bool CheckItemType(InteractiveItem item, Type[] availableItems)
    {
        bool result = false;

        foreach (Type type in availableItems)
        {
            if (item.GetType() == type)
            {
                result = true;
                break;
            }
        }

        return result;
    }
}
