using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindGenerator : ItemFunction
{
    public float windDistance;
    public float generateInterval;

    private bool isGenerating;
    private Vector2 itemPos;
    private Vector2Int itemSize;
    private Vector2 direction;
    private IEnumerator generateCorutine;

    private void Awake()
    {
        isGenerating = false;
    }

    public void GenerateStart(Vector3 pos, Vector2Int size, Vector2 dir)
    {
        itemPos = pos;
        itemSize = size;
        direction = dir;

        if (!isGenerating)
        {
            isGenerating = true;
            generateCorutine = GenerateWind();
            StartCoroutine(generateCorutine);
        }
    }

    public void GenerateStop()
    {
        if (isGenerating)
        {
            isGenerating = false;
            StopCoroutine(generateCorutine);
            StopWind();
        }
    }

    IEnumerator GenerateWind()
    {
        while (true)
        {
            Vector2 startPos = itemPos + new Vector2(
                (itemSize.x / 2.0f + 0.1f) * direction.x,
                (itemSize.y / 2.0f + 0.1f) * direction.y);
            float distance = windDistance;

            RaycastHit2D[] hits = Physics2D.RaycastAll(startPos, direction, distance);
            ItemFunction itemFunc = Utility.GetRaycastHitInfo<ItemFunction>(hits);
            InteractiveItem windCheckItem = null;
            WindChecker checker = null;

            // Ray의 경로 상에 WindChecker가 존재할 경우
            if (itemFunc != null && itemFunc is WindChecker windChcker)
            {
                windCheckItem = itemFunc.transform.parent.GetComponent<InteractiveItem>();
                checker = windChcker;

                PlayManager pm = GameObject.Find("PlayManager").GetComponent<PlayManager>();

                // 아이템이 GameZone에 존재하지 않을 경우
                if (!Utility.CheckItemBelongs(windCheckItem, pm.gameZone))
                {
                    windCheckItem = null;
                }
            }

            foreach (RaycastHit2D hit in hits)
            {
                // 경로 상의 아이템 슬롯의 바람 방향을 설정
                if (hit.collider.CompareTag("ItemSlot"))
                {
                    ItemSlot slot = hit.collider.GetComponent<ItemSlot>();

                    if (!slot.windSources.Contains(this))
                    {
                        slot.windSources.Add(this);
                    }
                }
                // 아이템과 충돌 시, 바람 설정 중단
                else if (hit.collider.CompareTag("InteractiveItem"))
                {
                    InteractiveItem item = hit.collider.GetComponent<InteractiveItem>();
                    ItemSlot slot = Utility.GetParentItemSlot(item);

                    if (slot.windSources.Contains(this))
                    {
                        slot.windSources.Remove(this);
                    }

                    // WindChecker를 가진 아이템과 충돌했을 경우, 바람 관련 동작 처리
                    if (item == windCheckItem)
                    {
                        checker.Charge();
                    }

                    break;
                }
            }

            yield return new WaitForSeconds(generateInterval);
        }
    }

    private void StopWind()
    {
        Vector2 startPos = itemPos + new Vector2(
                (itemSize.x / 2.0f + 0.1f) * direction.x,
                (itemSize.y / 2.0f + 0.1f) * direction.y);
        RaycastHit2D[] hits = Physics2D.RaycastAll(startPos, direction, windDistance);

        foreach (RaycastHit2D hit in hits)
        {
            if (hit.collider.CompareTag("ItemSlot"))
            {
                ItemSlot slot = hit.collider.GetComponent<ItemSlot>();

                if (slot.windSources.Contains(this))
                {
                    slot.windSources.Remove(this);
                }
            }
            else if (hit.collider.CompareTag("InteractiveItem"))
            {
                InteractiveItem item = hit.collider.GetComponent<InteractiveItem>();
                ItemSlot slot = Utility.GetParentItemSlot(item);

                if (slot.windSources.Contains(this))
                {
                    slot.windSources.Remove(this);
                }

                break;
            }
        }
    }

    public Vector2 GetWindDirection()
    {
        return direction;
    }
}
