using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightGenerator : ItemFunction
{
    public float lightDistance;
    public float generateInterval;

    private bool isGenerating;
    private Vector2 itemPos;
    private Vector2Int itemSize;
    private Vector2[] directions;
    private IEnumerator generateCorutine;

    private void Awake()
    {
        isGenerating = false;
    }

    public void GenerateStart(Vector3 pos, Vector2Int size, Vector2[] dir)
    {
        itemPos = pos;
        itemSize = size;
        directions = dir;

        if (!isGenerating)
        {
            isGenerating = true;
            generateCorutine = GenerateLight();
            StartCoroutine(generateCorutine);
        }
    }

    public void GenerateStop()
    {
        if (isGenerating)
        {
            isGenerating = false;
            StopCoroutine(generateCorutine);
        }
    }

    IEnumerator GenerateLight()
    {
        while (true)
        {
            foreach (Vector2 dir in directions)
            {
                Vector2 startPos = itemPos + new Vector2(
                    (itemSize.x / 2.0f + 0.1f) * dir.x,
                    (itemSize.y / 2.0f + 0.1f) * dir.y);
                RaycastHit2D[] hits = Physics2D.RaycastAll(startPos, dir, lightDistance);
                InteractiveItem item = Utility.GetRaycastHitInfo<InteractiveItem>(hits);

                PlayManager pm = GameObject.Find("PlayManager").GetComponent<PlayManager>();

                // 빛의 경로에 아이템이 존재하고, 해당 아이템이 게임존에 속한 아이템일 경우
                if (item != null && Utility.CheckItemBelongs(item, pm.gameZone))
                {
                    Transform checkerTransform = item.transform.Find("LightChecker");

                    if (checkerTransform != null)
                    {
                        LightChecker checker = checkerTransform.GetComponent<LightChecker>();
                        checker.Charge();
                    }
                }
            }

            yield return new WaitForSeconds(generateInterval);
        }
    }
}
