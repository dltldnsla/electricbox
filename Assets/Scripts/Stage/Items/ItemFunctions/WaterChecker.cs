using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterChecker : ItemFunction
{
    public delegate void OnCharged();
    public delegate void OnDischarged();

    public OnCharged onCharged;
    public OnDischarged onDischarged;

    public CheckerState state;

    public float checkInterval;         // 물 체크 간격
    public float chargeTime;            // 충전하는데 걸리는 시간
    public float dischargeTime;         // 방전되는데 걸리는 시간

    private bool isChecking;            // 물 체크 진행 여부
    private bool isWaterOn;             // 물을 공급받고 있는지 여부

    private IEnumerator checkCorutine;
    private IEnumerator chargeCorutine;
    private IEnumerator dischargeCorutine;

    private void Awake()
    {
        state = CheckerState.Discharged;

        isChecking = false;
    }

    private void FixedUpdate()
    {
        if (isChecking)
        {
            // 충전 처리
            if (isWaterOn)
            {
                if (state == CheckerState.Discharging || state == CheckerState.Discharged)
                {
                    if (state == CheckerState.Discharging)
                    {
                        StopCoroutine(dischargeCorutine);
                    }

                    chargeCorutine = ChargeWater();
                    StartCoroutine(chargeCorutine);
                }
            }
            // 방전 처리
            else
            {
                if (state == CheckerState.Charging)
                {
                    StopCoroutine(chargeCorutine);
                    state = CheckerState.Discharged;
                }
                else if (state == CheckerState.Charged)
                {
                    dischargeCorutine = DischargeWater();
                    StartCoroutine(dischargeCorutine);
                }
            }
        }
    }

    // 물을 공급받았을 경우 호출
    public void Charge()
    {
        if (isChecking)
        {
            isWaterOn = true;
            StopCoroutine(checkCorutine);

            checkCorutine = WaterSourceCheck();
            StartCoroutine(checkCorutine);
        }
    }

    // 물 체크 시작
    public void StartWaterCheck()
    {
        checkCorutine = WaterSourceCheck();
        StartCoroutine(checkCorutine);
    }

    // 물 체크 종료
    public void StopWaterCheck()
    {
        StopAllCoroutines();
        state = CheckerState.Discharged;
        isWaterOn = false;
        isChecking = false;
    }

    // 충전 체크
    IEnumerator ChargeWater()
    {
        state = CheckerState.Charging;

        yield return new WaitForSeconds(chargeTime);

        state = CheckerState.Charged;

        onCharged();
    }

    // 방전 체크
    IEnumerator DischargeWater()
    {
        state = CheckerState.Discharging;

        yield return new WaitForSeconds(dischargeTime);

        state = CheckerState.Discharged;

        onDischarged();
    }

    // 물 공급 체크
    IEnumerator WaterSourceCheck()
    {
        isChecking = true;

        while (true)
        {
            yield return new WaitForSeconds(checkInterval);

            isWaterOn = false;
        }
    }
}
