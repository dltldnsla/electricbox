using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemMagnet : ItemFunction
{
    public float searchDistance;
    public float moveSpeed;
   

    private bool isAvailable;               // 사용 가능 여부
    private InteractiveItem targetItem;     // 이동시킬 아이템
    private ItemSlot targetSlot;            // 아이템 이동 목적지

    private void Awake()
    {
        isAvailable = true;
        targetItem = null;
        targetSlot = null;
    }

    private void Update()
    {
        // 대상 아이템이 존재할 경우, 목표 슬롯을 향해 이동
        if (targetItem != null)
        {
            targetItem.transform.position = Vector3.MoveTowards(
                targetItem.transform.position,
                targetSlot.transform.position,
                moveSpeed * Time.deltaTime
                );

            // 목표 지점에 도착하였을 경우
            if (targetItem.transform.position == targetSlot.transform.position)
            {
                ItemSlotGrid slotGrid = Utility.GetParentItemSlotGrid(targetSlot);
                slotGrid.PlaceItem(targetSlot.slotCoordinate, targetItem);

                targetItem.StartFunctions();

                targetItem = null;
                targetSlot = null;
            }
        }
    }

    public void TransportItem(Vector3 pos, Vector2Int size, Vector2 dir, ItemSlot slot, Type[] availableItems)
    {
        if (targetItem == null && isAvailable)
        {
            Vector2 startPos = (Vector2)pos + new Vector2(
                (size.x / 2.0f + 0.1f) * dir.x,
                (size.y / 2.0f + 0.1f) * dir.y);

            RaycastHit2D[] hits = Physics2D.RaycastAll(startPos, dir, searchDistance);
            InteractiveItem item = Utility.GetRaycastHitInfo<InteractiveItem>(hits);

            // Ray의 경로 상에 이동 가능한 아이템이 존재할 경우
            if (item != null && CheckItemType(item, availableItems))
            {
                PlayManager pm = GameObject.Find("PlayManager").GetComponent<PlayManager>();

                // 아이템이 GameZone에 존재하는 경우에만 이동
                if (Utility.CheckItemBelongs(item, pm.gameZone))
                {
                    targetItem = item;
                    targetSlot = slot;
                    isAvailable = false;

                    ItemSlot targetItemSlot = Utility.GetParentItemSlot(item);
                    ItemSlotGrid slotGrid = Utility.GetParentItemSlotGrid(targetItemSlot);

                    item.StopFunctions();
                    slotGrid.RemoveItem(targetItemSlot.slotCoordinate, item);
                }
            }
        }
    }

    public void SetAvailable(bool val)
    {
        isAvailable = val;
    }

    private bool CheckItemType(InteractiveItem item, Type[] availableItems)
    {
        bool result = false;

        foreach(Type type in availableItems)
        {
            if(item.GetType() == type)
            {
                result = true;
                break;
            }
        }

        return result;
    }
}
