using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterGenerator : ItemFunction
{
    public float generateInterval;
    public Water water;

    private bool isGenerating;
    private Vector3 generatePos;
    private IEnumerator generateCorutine;

    private void Awake()
    {
        isGenerating = false;
    }

    public void GenerateStart(Vector3 genPos)
    {
        generatePos = genPos;

        if (!isGenerating)
        {
            isGenerating = true;
            generateCorutine = GenerateWater();
            StartCoroutine(generateCorutine);
        }
    }

    public void GenerateStop()
    {
        if (isGenerating)
        {
            isGenerating = false;
            StopCoroutine(generateCorutine);
        }
    }

    IEnumerator GenerateWater()
    {
        while (true)
        {
            Water waterObj = Instantiate(water, generatePos, Quaternion.identity);
            Physics2D.IgnoreCollision(waterObj.transform.GetComponent<Collider2D>(), transform.parent.GetComponent<Collider2D>());

            yield return new WaitForSeconds(generateInterval);
        }
    }
}
