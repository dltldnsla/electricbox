using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserChecker : ItemFunction
{
    public delegate void OnCharged();
    public delegate void OnDischarged();

    public OnCharged onCharged;
    public OnDischarged onDischarged;

    public CheckerState state;

    public float checkInterval;         // 레이저 체크 간격
    public float chargeTime;            // 충전하는데 걸리는 시간
    public float dischargeTime;         // 방전되는데 걸리는 시간

    private bool isChecking;            // 레이저 체크 진행 여부
    private bool isLaserOn;             // 레이저를 공급받고 있는지 여부

    private IEnumerator checkCorutine;
    private IEnumerator chargeCorutine;
    private IEnumerator dischargeCorutine;

    private void Awake()
    {
        state = CheckerState.Discharged;

        isChecking = false;
    }

    private void FixedUpdate()
    {
        if (isChecking)
        {
            // 충전 처리
            if (isLaserOn)
            {
                if (state == CheckerState.Discharging || state == CheckerState.Discharged)
                {
                    if (state == CheckerState.Discharging)
                    {
                        StopCoroutine(dischargeCorutine);
                    }

                    chargeCorutine = ChargeLaser();
                    StartCoroutine(chargeCorutine);
                }
            }
            // 방전 처리
            else
            {
                if (state == CheckerState.Charging)
                {
                    StopCoroutine(chargeCorutine);
                    state = CheckerState.Discharged;
                }
                else if (state == CheckerState.Charged)
                {
                    dischargeCorutine = DischargeLaser();
                    StartCoroutine(dischargeCorutine);
                }
            }
        }
    }

    // 레이저를 공급받았을 경우 호출
    public void Charge()
    {
        if (isChecking)
        {
            isLaserOn = true;
            StopCoroutine(checkCorutine);

            checkCorutine = LaserSourceCheck();
            StartCoroutine(checkCorutine);
        }
    }

    // 레이저 체크 시작
    public void StartLaserCheck()
    {
        checkCorutine = LaserSourceCheck();
        StartCoroutine(checkCorutine);
    }

    // 레이저 체크 종료
    public void StopLaserCheck()
    {
        StopAllCoroutines();
        state = CheckerState.Discharged;
        isLaserOn = false;
        isChecking = false;
    }

    // 충전 체크
    IEnumerator ChargeLaser()
    {
        state = CheckerState.Charging;

        yield return new WaitForSeconds(chargeTime);

        state = CheckerState.Charged;

        onCharged();
    }

    // 방전 체크
    IEnumerator DischargeLaser()
    {
        state = CheckerState.Discharging;

        yield return new WaitForSeconds(dischargeTime);

        state = CheckerState.Discharged;

        onDischarged();
    }

    // 레이저 공급 체크
    IEnumerator LaserSourceCheck()
    {
        isChecking = true;

        while (true)
        {
            yield return new WaitForSeconds(checkInterval);

            isLaserOn = false;
        }
    }
}
