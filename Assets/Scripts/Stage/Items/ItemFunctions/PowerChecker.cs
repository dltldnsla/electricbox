using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerChecker : ItemFunction
{
    public delegate void OnChargeStart();
    public delegate void OnCharged(Vector2Int powerSourcePos);
    public delegate void OnDischarged();

    public OnChargeStart onChargeStart;
    public OnCharged onCharged;
    public OnDischarged onDischarged;

    public CheckerState state;

    public float checkInterval;         // 바람 체크 간격
    public float chargeTime;            // 충전하는데 걸리는 시간
    public float dischargeTime;         // 방전되는데 걸리는 시간

    private bool isChecking;            // 전원 체크 진행 여부
    private bool isPowerOn;             // 전원 공급을 받고 있는지 여부

    private Vector2Int powerSourcePos;  // 전원이 공급되는 슬롯의 위치

    private IEnumerator checkCorutine;
    private IEnumerator chargeCorutine;
    private IEnumerator dischargeCorutine;

    private void Awake()
    {
        state = CheckerState.Discharged;

        isChecking = false;
        powerSourcePos = -Vector2Int.one;
    }

    private void FixedUpdate()
    {
        if (isChecking)
        {
            // 충전 처리
            if (isPowerOn)
            {
                if (state == CheckerState.Discharging || state == CheckerState.Discharged)
                {
                    if (state == CheckerState.Discharging)
                    {
                        StopCoroutine(dischargeCorutine);
                    }

                    chargeCorutine = ChargePower();
                    StartCoroutine(chargeCorutine);

                    onChargeStart();
                }
            }
            // 방전 처리
            else
            {
                if (state == CheckerState.Charging)
                {
                    StopCoroutine(chargeCorutine);
                    state = CheckerState.Discharged;
                }
                else if (state == CheckerState.Charged)
                {
                    dischargeCorutine = DischargePower();
                    StartCoroutine(dischargeCorutine);
                }
            }
        } 
    }

    // 전원을 공급받았을 경우 호출
    public void Charge(Vector2Int powSrcPos)
    {
        if (isChecking)
        {
            if (isPowerOn)
            {
                // 서로 다른 오브젝트로부터 동시에 전원을 공급받을 경우, 먼저 공급한 오브젝트만 체크
                if (powSrcPos == powerSourcePos)
                {
                    StopCoroutine(checkCorutine);

                    checkCorutine = PowerSourceCheck();
                    StartCoroutine(checkCorutine);
                }
            }
            else
            {
                // 전원을 공급받고 있지 않았을 경우
                isPowerOn = true;
                powerSourcePos = powSrcPos;

                StopCoroutine(checkCorutine);

                checkCorutine = PowerSourceCheck();
                StartCoroutine(checkCorutine);
            }
        }
    }

    // 전원 공급 체크 시작
    public void StartPowerCheck()
    {
        checkCorutine = PowerSourceCheck();
        StartCoroutine(checkCorutine);
    }

    // 전원 공급 체크 종료
    public void StopPowerCheck()
    {
        StopAllCoroutines();
        state = CheckerState.Discharged;
        isPowerOn = false;
        isChecking = false;
        powerSourcePos = -Vector2Int.one;
    }

    // 충전 체크
    IEnumerator ChargePower()
    {
        state = CheckerState.Charging;

        yield return new WaitForSeconds(chargeTime);

        state = CheckerState.Charged;

        onCharged(powerSourcePos);
    }

    // 방전 체크
    IEnumerator DischargePower()
    {
        powerSourcePos = -Vector2Int.one;
        state = CheckerState.Discharging;

        yield return new WaitForSeconds(dischargeTime);

        state = CheckerState.Discharged;

        onDischarged();
    }

    // 전원 공급 체크
    IEnumerator PowerSourceCheck()
    {
        isChecking = true;

        while (true)
        {
            yield return new WaitForSeconds(checkInterval);

            isPowerOn = false;
        }
    }
}
