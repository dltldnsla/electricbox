using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteamGenerator : ItemFunction
{
    public float generateInterval;
    public Steam steam;

    private bool isGenerating;
    private ItemSlot generateSlot;
    private IEnumerator generateCorutine;

    private void Awake()
    {
        isGenerating = false;
    }

    public void GenerateStart(ItemSlot genSlot)
    {
        generateSlot = genSlot;

        if (!isGenerating)
        {
            isGenerating = true;
            generateCorutine = GenerateSteam();
            StartCoroutine(generateCorutine);
        }
    }

    public void GenerateStop()
    {
        if (isGenerating)
        {
            isGenerating = false;
            StopCoroutine(generateCorutine);
        }
    }

    IEnumerator GenerateSteam()
    {
        while (generateSlot != null)
        {
            Vector3 generatePos = generateSlot.transform.position + new Vector3(0, -0.1f, 0);

            Steam steamObj = Instantiate(steam, generatePos, Quaternion.identity);
            //steamObj.SetDestination(generateSlot);
            steamObj.Initiate(generateSlot);
            Physics2D.IgnoreCollision(steamObj.transform.GetComponent<Collider2D>(), transform.parent.GetComponent<Collider2D>());

            yield return new WaitForSeconds(generateInterval);
        }
    }
}
