using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserGenerator : ItemFunction
{
    public float generateInterval;
    public Laser laser;

    private bool isGenerating;
    private Vector2Int direction;
    private ItemSlot generateSlot;
    private IEnumerator generateCorutine;

    private void Awake()
    {
        isGenerating = false;
    }

    public void GenerateStart(ItemSlot genSlot, Vector2Int dir)
    {
        generateSlot = genSlot;
        direction = dir;

        if (!isGenerating)
        {
            isGenerating = true;
            generateCorutine = GenerateLaser();
            StartCoroutine(generateCorutine);
        }
    }

    public void GenerateStop()
    {
        if (isGenerating)
        {
            isGenerating = false;
            StopCoroutine(generateCorutine);
        }
    }

    IEnumerator GenerateLaser()
    {
        while (generateSlot != null)
        {
            Vector3 generatePos = generateSlot.transform.position + new Vector3(-0.4f * direction.x, 0, 0);

            Laser laserObj = Instantiate(laser, generatePos, Quaternion.identity);
            laserObj.Initiate(generateSlot, direction);
            Physics2D.IgnoreCollision(laserObj.transform.GetComponent<Collider2D>(), transform.parent.GetComponent<Collider2D>());

            yield return new WaitForSeconds(generateInterval);
        }
    }
}
