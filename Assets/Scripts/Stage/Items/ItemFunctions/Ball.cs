using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Ball : ItemFunction
{
    public float speed;
    public AudioSource crashSound;

    private Vector2 blowDir;
    private bool isMoving;

    private void Start()
    {
        blowDir = Vector2.left;
        isMoving = false;
    }

    private void Update()
    {
        if (isMoving)
        {
            transform.position += Vector3.down * speed * Time.deltaTime;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // 스스로 이동하고 있지 않을 경우, 처리 안함
        if (!isMoving)
        {
            return;
        }

        InteractiveItem parentItem = transform.parent.GetComponent<InteractiveItem>();
        ItemSlot slot = collision.GetComponent<ItemSlot>();

        if (slot != null)
        {
            InteractiveItem slotItem = slot.containedItem;

            // 지나가는 슬롯에 아이템이 있을 경우, 해당 아이템을 날려버림
            if (slotItem != null && slotItem != parentItem)
            {
                slotItem.StopFunctions();
                slotItem.gameObject.SetActive(false);

                //slotItem.BlowAway(blowDir);
                //blowDir = -blowDir;

                ItemSlotGrid itemSlotGrid = Utility.GetParentItemSlotGrid(slot);
                itemSlotGrid.RemoveItem(slot.slotCoordinate, slot.containedItem);

                crashSound.Play();
            }
        }
    }

    public void StartMove()
    {
        isMoving = true;
    }

    public void StopMove()
    {
        isMoving = false;
    }
}
