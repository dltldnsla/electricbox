using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindChecker : ItemFunction
{
    public delegate void OnCharged();
    public delegate void OnDischarged();

    public OnCharged onCharged;
    public OnDischarged onDischarged;

    public CheckerState state;

    public float checkInterval;         // 바람 체크 간격
    public float chargeTime;            // 충전하는데 걸리는 시간
    public float dischargeTime;         // 방전되는데 걸리는 시간

    private bool isChecking;            // 바람 체크 진행 여부
    private bool isWindOn;             // 바람을 공급받고 있는지 여부

    private IEnumerator checkCorutine;
    private IEnumerator chargeCorutine;
    private IEnumerator dischargeCorutine;

    private void Awake()
    {
        state = CheckerState.Discharged;

        isChecking = false;
    }

    private void FixedUpdate()
    {
        if (isChecking)
        {
            // 충전 처리
            if (isWindOn)
            {
                if (state == CheckerState.Discharging || state == CheckerState.Discharged)
                {
                    if (state == CheckerState.Discharging)
                    {
                        StopCoroutine(dischargeCorutine);
                    }

                    chargeCorutine = ChargeWind();
                    StartCoroutine(chargeCorutine);
                }
            }
            // 방전 처리
            else
            {
                if (state == CheckerState.Charging)
                {
                    StopCoroutine(chargeCorutine);
                    state = CheckerState.Discharged;
                }
                else if (state == CheckerState.Charged)
                {
                    dischargeCorutine = DischargeWind();
                    StartCoroutine(dischargeCorutine);
                }
            }
        }
    }

    // 바람을 공급받았을 경우 호출
    public void Charge()
    {
        if (isChecking)
        {
            isWindOn = true;
            StopCoroutine(checkCorutine);

            checkCorutine = WindSourceCheck();
            StartCoroutine(checkCorutine);
        }
    }

    // 바람 체크 시작
    public void StartWindCheck()
    {
        checkCorutine = WindSourceCheck();
        StartCoroutine(checkCorutine);
    }

    // 바람 체크 종료
    public void StopWindCheck()
    {
        StopAllCoroutines();
        state = CheckerState.Discharged;
        isWindOn = false;
        isChecking = false;
    }

    // 충전 체크
    IEnumerator ChargeWind()
    {
        state = CheckerState.Charging;

        yield return new WaitForSeconds(chargeTime);

        state = CheckerState.Charged;

        onCharged();
    }

    // 방전 체크
    IEnumerator DischargeWind()
    {
        state = CheckerState.Discharging;

        yield return new WaitForSeconds(dischargeTime);

        state = CheckerState.Discharged;

        onDischarged();
    }

    // 바람 공급 체크
    IEnumerator WindSourceCheck()
    {
        isChecking = true;

        while (true)
        {
            yield return new WaitForSeconds(checkInterval);

            isWindOn = false;
        }
    }
}
