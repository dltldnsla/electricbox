using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// ToDo : 동작 이상 체크(inactive)
public class SteamChecker : ItemFunction
{
    public delegate void OnCharged();
    public delegate void OnDischarged();

    public OnCharged onCharged;
    public OnDischarged onDischarged;

    public CheckerState state;

    public float checkInterval;         // 증기 체크 간격
    public float chargeTime;            // 충전하는데 걸리는 시간
    public float dischargeTime;         // 방전되는데 걸리는 시간

    private bool isChecking;            // 증기 체크 진행 여부
    private bool isSteamOn;             // 증기를 공급받고 있는지 여부

    private IEnumerator checkCorutine;
    private IEnumerator chargeCorutine;
    private IEnumerator dischargeCorutine;

    private void Awake()
    {
        state = CheckerState.Discharged;

        isChecking = false;
    }

    private void FixedUpdate()
    {
        if (isChecking)
        {
            // 충전 처리
            if (isSteamOn)
            {
                if (state == CheckerState.Discharging || state == CheckerState.Discharged)
                {
                    if (state == CheckerState.Discharging)
                    {
                        StopCoroutine(dischargeCorutine);
                    }

                    chargeCorutine = ChargeSteam();
                    StartCoroutine(chargeCorutine);
                }
            }
            // 방전 처리
            else
            {
                if (state == CheckerState.Charging)
                {
                    StopCoroutine(chargeCorutine);
                    state = CheckerState.Discharged;
                }
                else if (state == CheckerState.Charged)
                {
                    dischargeCorutine = DischargeSteam();
                    StartCoroutine(dischargeCorutine);
                }
            }
        }
    }

    // 증기를 공급받았을 경우 호출
    public void Charge()
    {
        if (isChecking)
        {
            isSteamOn = true;
            StopCoroutine(checkCorutine);

            checkCorutine = SteamSourceCheck();
            StartCoroutine(checkCorutine);
        }
    }

    // 증기 체크 시작
    public void StartSteamCheck()
    {
        checkCorutine = SteamSourceCheck();
        StartCoroutine(checkCorutine);
    }

    // 증기 체크 종료
    public void StopSteamCheck()
    {
        StopAllCoroutines();
        state = CheckerState.Discharged;
        isSteamOn = false;
        isChecking = false;
    }

    // 충전 체크
    IEnumerator ChargeSteam()
    {
        state = CheckerState.Charging;

        yield return new WaitForSeconds(chargeTime);

        state = CheckerState.Charged;

        onCharged();
    }

    // 방전 체크
    IEnumerator DischargeSteam()
    {
        state = CheckerState.Discharging;

        yield return new WaitForSeconds(dischargeTime);

        state = CheckerState.Discharged;

        onDischarged();
    }

    // 증기 공급 체크
    IEnumerator SteamSourceCheck()
    {
        isChecking = true;

        while (true)
        {
            yield return new WaitForSeconds(checkInterval);

            isSteamOn = false;
        }
    }
}
