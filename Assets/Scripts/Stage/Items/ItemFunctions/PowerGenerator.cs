using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerGenerator : ItemFunction
{
    public Vector2Int generatorPos;     // 부모 아이템을 기준으로 PowerGenerater가 위치한 슬롯 좌표
    public float generateInterval;

    private bool isGenerating;
    private IEnumerator generateCorutine;

    private void Awake()
    {
        isGenerating = false;
    }

    public void GenerateStart()
    {
        if (!isGenerating)
        {
            isGenerating = true;
            generateCorutine = GeneratePower();
            StartCoroutine(generateCorutine);
        }
    }

    public void GenerateStop()
    {
        if (isGenerating)
        {
            isGenerating = false;
            StopCoroutine(generateCorutine);
        }
    }

    IEnumerator GeneratePower()
    {
        while (true)
        {
            // 제너레이터가 위치한 슬롯 탐색
            Item item = transform.parent.GetComponent<Item>();
            ItemSlot itemSlot = Utility.GetParentItemSlot(item);
            ItemSlotGrid slotGrid = Utility.GetParentItemSlotGrid(itemSlot);

            Vector2Int itemSlotPos = itemSlot.slotCoordinate;
            Vector2Int currentSlotPos = itemSlotPos + generatorPos;
            ItemSlot currentSlot = slotGrid.GetItemSlot(currentSlotPos);

            // 탐색한 슬롯에 있는 전선에 전원 공급
            foreach (ConnectionWire wire in currentSlot.connectedWires)
            {
                if (item is ConnectionWire connectionWire)
                {
                    // 전원을 공급하는 전선 자신은 대상에서 제외
                    if (wire == connectionWire)
                    {
                        continue;
                    }
                }

                Transform checkerTransform = wire.transform.Find("PowerChecker");

                if (checkerTransform != null)
                {
                    PowerChecker checker = checkerTransform.GetComponent<PowerChecker>();
                    checker.Charge(currentSlotPos);
                }
            }

            // 제너레이터를 가진 아이템이 전선일 경우, 해당 슬롯의 아이템에 전원 공급
            if (item is ConnectionWire)
            {
                InteractiveItem containedItem = currentSlot.containedItem;

                if(containedItem != null)
                {
                    Transform checkerTransform = containedItem.transform.Find("PowerChecker");

                    if (checkerTransform != null)
                    {
                        PowerChecker checker = checkerTransform.GetComponent<PowerChecker>();
                        checker.Charge(currentSlotPos);
                    }
                }
            }

            yield return new WaitForSeconds(generateInterval);
        }
    }
}
