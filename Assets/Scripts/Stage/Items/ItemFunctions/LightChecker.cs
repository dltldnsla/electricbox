using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightChecker : ItemFunction
{
    public delegate void OnCharged();
    public delegate void OnDischarged();

    public OnCharged onCharged;
    public OnDischarged onDischarged;

    public CheckerState state;

    public float checkInterval;         // 조명 체크 간격
    public float chargeTime;            // 충전하는데 걸리는 시간
    public float dischargeTime;         // 방전되는데 걸리는 시간

    private bool isChecking;            // 조명 체크 진행 여부
    private bool isLightOn;             // 조명을 받고 있는지 여부

    private IEnumerator checkCorutine;
    private IEnumerator chargeCorutine;
    private IEnumerator dischargeCorutine;

    private void Awake()
    {
        state = CheckerState.Discharged;

        isChecking = false;
    }

    private void FixedUpdate()
    {
        if (isChecking)
        {
            // 충전 처리
            if (isLightOn)
            {
                if (state == CheckerState.Discharging || state == CheckerState.Discharged)
                {
                    if (state == CheckerState.Discharging)
                    {
                        StopCoroutine(dischargeCorutine);
                    }

                    chargeCorutine = ChargeLight();
                    StartCoroutine(chargeCorutine);
                }
            }
            // 방전 처리
            else
            {
                if (state == CheckerState.Charging)
                {
                    StopCoroutine(chargeCorutine);
                    state = CheckerState.Discharged;
                }
                else if (state == CheckerState.Charged)
                {
                    dischargeCorutine = DischargeLight();
                    StartCoroutine(dischargeCorutine);
                }
            }
        }
    }

    // 조명으로부터 빛을 받았을 경우 호출
    public void Charge()
    {
        if (isChecking)
        {
            isLightOn = true;
            StopCoroutine(checkCorutine);

            checkCorutine = LightSourceCheck();
            StartCoroutine(checkCorutine);
        }
    }

    // 조명 체크 시작
    public void StartLightCheck()
    {
        checkCorutine = LightSourceCheck();
        StartCoroutine(checkCorutine);
    }

    // 조명 체크 종료
    public void StopLightCheck()
    {
        StopAllCoroutines();
        state = CheckerState.Discharged;
        isLightOn = false;
        isChecking = false;
    }

    // 충전 체크
    IEnumerator ChargeLight()
    {
        state = CheckerState.Charging;

        yield return new WaitForSeconds(chargeTime);

        state = CheckerState.Charged;

        onCharged();
    }

    // 방전 체크
    IEnumerator DischargeLight()
    {
        state = CheckerState.Discharging;

        yield return new WaitForSeconds(dischargeTime);

        state = CheckerState.Discharged;

        onDischarged();
    }

    // 전원 공급 체크
    IEnumerator LightSourceCheck()
    {
        isChecking = true;

        while (true)
        {
            yield return new WaitForSeconds(checkInterval);

            isLightOn = false;
        }
    }
}
