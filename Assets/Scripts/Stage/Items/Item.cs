using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public abstract class Item : MonoBehaviour
{
    public string itemName;
    public string itemDescription;

    public bool isDraggable;            // 드래그 가능 여부 
    public Vector2Int itemSize;

    public Animator animator;

    private Color32 defaultColor;

    private void Awake()
    {
        defaultColor = transform.GetComponent<SpriteRenderer>().color;

        BoxCollider2D boxCollider = transform.GetComponent<BoxCollider2D>();
        itemSize = new Vector2Int((int)boxCollider.size.x, (int)boxCollider.size.y);
    }

    // 아이템의 기능 동작을 시작
    public abstract void StartFunctions();

    // 아이템의 기능 동작을 중지
    public abstract void StopFunctions();

    // 아이템의 중심점으로부터 아이템의 좌측 상단 칸의 중심 좌표로의 벡터 값을 반환
    public Vector3 GetFirstSlotPos()
    {
        return new Vector3(
            (itemSize.x - 1) / 2.0f,
            (itemSize.y - 1) / 2.0f * (-1),
            0);
    }

    // 드래그 여부에 따라 아이템 투명도를 조절
    public void SetTransparent(bool isSelected)
    {
        Color32 color = defaultColor;

        // 아이템이 선택되었을 경우, 투명도를 절반으로 함
        if (isSelected)
        {
            color = new Color32(
                defaultColor.r,
                defaultColor.g,
                defaultColor.b,
                (byte)(defaultColor.a / 2));
        }

        transform.GetComponent<SpriteRenderer>().color = color;
    }

    public void BlowAway(Vector2 dir)
    {
        if (dir == Vector2.left)
        {
            animator.SetTrigger("blowLeft");
        }
        else if (dir == Vector2.right)
        {
            animator.SetTrigger("blowRight");
        }
    }    
}