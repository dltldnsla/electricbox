using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CheckerState
{
    Discharged,
    Discharging,
    Charging,
    Charged,
}

// 아이템 기능 오브젝트를 분류하기 위한 클래스
public abstract class ItemFunction : MonoBehaviour
{

}