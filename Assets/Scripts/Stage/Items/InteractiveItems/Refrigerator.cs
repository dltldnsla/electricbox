using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Refrigerator : InteractiveItem
{
    public PowerChecker powerChecker;
    public SteamChecker steamChecker;
    public WaterGenerator waterGenerator;

    private bool isRunning;
    private bool isSteamCharged;
    private bool isGenerating;

    private void Start()
    {
        powerChecker.onChargeStart = OnPowerChargeStart;
        powerChecker.onCharged = OnPowerCharged;
        powerChecker.onDischarged = OnPowerDischarged;

        steamChecker.onCharged = OnSteamCharged;
        steamChecker.onDischarged = OnSteamDischarged;

        isRunning = false;
        isSteamCharged = false;
        isGenerating = false;
    }

    private void Update()
    {
        // 물 생산 조건을 만족할 경우
        if (!isGenerating)
        {
            if(isRunning && isSteamCharged)
            {
                Vector3 genPos = transform.position;

                if(transform.localScale.x > 0)
                {
                    genPos += new Vector3(0.5f, itemSize.y / 2.0f * (-1), 0);
                }
                else
                {
                    genPos += new Vector3(-0.5f, itemSize.y / 2.0f * (-1), 0);
                }

                isGenerating = true;
                waterGenerator.GenerateStart(genPos);

                animator.SetBool("isRunning", true);
            }
        }
        // 물 생산 조건을 만족하지 않을 경우
        else
        {
            if(!isRunning || !isSteamCharged)
            {
                isGenerating = false;
                waterGenerator.GenerateStop();

                animator.SetBool("isRunning", false);
            }
        }
    }

    private void OnPowerChargeStart() { }

    private void OnPowerCharged(Vector2Int powerSourcePos)
    {
        isRunning = true;
    }

    private void OnPowerDischarged()
    {
        isRunning = false;
    }

    private void OnSteamCharged()
    {
        isSteamCharged = true;
    }

    private void OnSteamDischarged()
    {
        isSteamCharged = false;
    }

    public override void OnMouseClick()
    {
        transform.localScale = new Vector3(
            transform.localScale.x * (-1),
            transform.localScale.y,
            transform.localScale.z);
    }

    public override void StartFunctions()
    {
        powerChecker.StartPowerCheck();
        steamChecker.StartSteamCheck();
    }

    public override void StopFunctions()
    {
        powerChecker.StopPowerCheck();
        steamChecker.StopSteamCheck();
        waterGenerator.GenerateStop();

        animator.SetBool("isRunning", false);
    }
}
