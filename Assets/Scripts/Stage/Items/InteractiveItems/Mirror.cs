using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mirror : InteractiveItem
{
    public Vector2 faceTo;          // 거울이 바라보고 있는 방향 벡터

    private Vector2 normalVector;   // 법선 벡터(정규화)

    private void Start()
    {
        normalVector = faceTo.normalized;
    }

    // 레이저가 반사 가능한 각도로 들어오는지 체크
    public bool ReflectCheck(Vector2 direction)
    {
        float angle = Vector2.Angle(normalVector, direction);

        if (angle > 90)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public Vector2 GetReflectVector(Vector2 direction)
    {
        return Vector2.Reflect(direction, normalVector);
    }

    public override void OnMouseClick()
    {
        transform.localScale = new Vector3(
            transform.localScale.x * (-1),
            transform.localScale.y,
            transform.localScale.z);

        normalVector = new Vector2(-normalVector.x, normalVector.y);
    }

    public override void StartFunctions() { }

    public override void StopFunctions() { }
}
