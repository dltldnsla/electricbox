using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargerLight : InteractiveItem
{
    public PowerChecker powerChecker;
    public LightGenerator lightGenerator;

    private void Start()
    {
        powerChecker.onChargeStart = OnPowerChargeStart;
        powerChecker.onCharged = OnPowerCharged;
        powerChecker.onDischarged = OnPowerDischarged;
    }

    private void OnPowerChargeStart() { }

    private void OnPowerCharged(Vector2Int powerSourcePos) { }

    private void OnPowerDischarged()
    {
        float dirX = transform.localScale.x > 0 ? 1 : -1;
        float dirY = 0;
        Vector2[] dir = { new Vector2(dirX, dirY) };

        lightGenerator.GenerateStart(transform.position, itemSize, dir);
        animator.SetBool("isRunning", true);
    }

    public override void OnMouseClick()
    {
        transform.localScale = new Vector3(
            transform.localScale.x * (-1),
            transform.localScale.y,
            transform.localScale.z);
    }

    public override void StartFunctions()
    {
        powerChecker.StartPowerCheck();
    }

    public override void StopFunctions()
    {
        powerChecker.StopPowerCheck();
        lightGenerator.GenerateStop();
        animator.SetBool("isRunning", false);
    }
}
