using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolarPanel : InteractiveItem
{
    public LightChecker lightChecker;
    public PowerGenerator powerGenerator;

    private void Start()
    {
        lightChecker.onCharged = OnLightCharged;
        lightChecker.onDischarged = OnLightDischarged;
    }

    private void OnLightCharged()
    {
        powerGenerator.GenerateStart();
    }

    private void OnLightDischarged()
    {
        powerGenerator.GenerateStop();
    }

    public override void OnMouseClick() { }

    public override void StartFunctions()
    {
        lightChecker.StartLightCheck();
    }

    public override void StopFunctions()
    {
        lightChecker.StopLightCheck();
        powerGenerator.GenerateStop();
    }
}
