using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class ElectricKettle : InteractiveItem
{
    public PowerChecker powerChecker;
    public SteamGenerator steamGenerator;
    public AudioSource boilingSound;

    private void Start()
    {
        powerChecker.onChargeStart = OnPowerChargeStart;
        powerChecker.onCharged = OnPowerCharged;
        powerChecker.onDischarged = OnPowerDischarged;
    }

    private void OnPowerChargeStart()
    {
        animator.SetTrigger("startRunning");
        boilingSound.Play();
    }

    private void OnPowerCharged(Vector2Int powerSourcePos)
    {
        ItemSlot genSlot = Utility.GetParentItemSlot(this);
        ItemSlotGrid gameZone = Utility.GetParentItemSlotGrid(genSlot);

        int dirX = transform.localScale.x > 0 ? 1 : -1;
        genSlot = gameZone.GetItemSlot(genSlot.slotCoordinate + new Vector2Int(dirX, 0));

        steamGenerator.GenerateStart(genSlot);
    }

    private void OnPowerDischarged()
    {
        steamGenerator.GenerateStop();
    }

    public override void OnMouseClick()
    {
        transform.localScale = new Vector3(
            transform.localScale.x * (-1),
            transform.localScale.y,
            transform.localScale.z);
    }

    public override void StartFunctions()
    {
        powerChecker.StartPowerCheck();
    }

    public override void StopFunctions()
    {
        powerChecker.StopPowerCheck();
        steamGenerator.GenerateStop();
    }
}
