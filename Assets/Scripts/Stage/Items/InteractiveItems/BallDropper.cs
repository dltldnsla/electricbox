using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallDropper : InteractiveItem
{
    public PowerChecker powerChecker;
    public Ball ball;

    private Vector3 ballPos;

    private void Start()
    {
        powerChecker.onChargeStart = OnPowerChargeStart;
        powerChecker.onCharged = OnPowerCharged;
        powerChecker.onDischarged = OnPowerDischarged;

        ballPos = ball.transform.position - transform.position;
    }

    private void OnPowerChargeStart() { }

    private void OnPowerCharged(Vector2Int powerSourcePos)
    {
        ball.StartMove();
    }

    private void OnPowerDischarged()
    {
        ball.StopMove();
        ball.transform.position = transform.position + ballPos;
    }

    public override void OnMouseClick() { }

    public override void StartFunctions()
    {
        powerChecker.StartPowerCheck();
    }

    public override void StopFunctions()
    {
        powerChecker.StopPowerCheck();

        ball.StopMove();
        ball.transform.position = transform.position + ballPos;
    }
}
