using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class TransporterBot : InteractiveItem
{
    public PowerChecker powerChecker;
    public ItemTransporter itemTransporter;
    public AudioSource engineSound;

    private Type[] availableItems;

    private void Start()
    {
        powerChecker.onChargeStart = OnPowerChargeStart;
        powerChecker.onCharged = OnPowerCharged;
        powerChecker.onDischarged = OnPowerDischarged;

        itemTransporter.onTransportStart = OnTransportStart;
        itemTransporter.onTransportFinished = OnTransportFinished;

        // ToDo : 각 아이템 구현 완료시마다 주석 해제
        availableItems = new Type[] {
            typeof(WaterDispenser),
            typeof(WaterTurbine),
            typeof(IPSBattery),
            typeof(Fan),
            typeof(SteamDetector),
            typeof(LightBulb),
            typeof(ChargerLight),
            typeof(Mirror),
            typeof(SolarPanel),
            typeof(ElectricKettle),
            typeof(BallDropper),
            typeof(LaserDetector),
            typeof(LaserDevice),
            typeof(Block),
        };
    }

    private void OnPowerChargeStart() { }

    private void OnPowerCharged(Vector2Int powerSourcePos)
    {
        int dirX = transform.localScale.x > 0 ? -1 : 1;
        int dirY = 0;
        Vector2Int moveDir = new Vector2Int(dirX, dirY);
        Vector2Int targetDir = new Vector2Int(0, -1);

        itemTransporter.TransportItem(moveDir, targetDir, availableItems);
    }

    private void OnPowerDischarged() { }

    private void OnTransportStart()
    {
        animator.SetBool("isRunning", true);
        engineSound.Play();
    }

    private void OnTransportFinished()
    {
        animator.SetBool("isRunning", false);
    }

    public override void OnMouseClick()
    {
        transform.localScale = new Vector3(
            transform.localScale.x * (-1),
            transform.localScale.y,
            transform.localScale.z);
    }

    public override void StartFunctions()
    {
        powerChecker.StartPowerCheck();
    }

    public override void StopFunctions()
    {
        powerChecker.StopPowerCheck();
        animator.SetBool("isRunning", false);
    }
}
