using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectroMagnet : InteractiveItem
{
    public PowerChecker powerChecker;
    public ItemMagnet itemMagnet;

    private Type[] availableItems;

    private void Start()
    {
        powerChecker.onChargeStart = OnPowerChargeStart;
        powerChecker.onCharged = OnPowerCharged;
        powerChecker.onDischarged = OnPowerDischarged;

        availableItems = new Type[] { 
            typeof(ElectricKettle),
            typeof(SteamDetector),
            typeof(Mirror),
        };
    }

    private void OnPowerChargeStart() { }

    private void OnPowerCharged(Vector2Int powerSourcePos)
    {
        float dirX = transform.localScale.x > 0 ? 1 : -1;
        float dirY = 0;
        Vector2 dir = new Vector2(dirX, dirY);

        ItemSlot currentSlot = Utility.GetParentItemSlot(this);
        ItemSlotGrid slotGrid = Utility.GetParentItemSlotGrid(currentSlot);
        ItemSlot targetSlot = slotGrid.GetItemSlot(currentSlot.slotCoordinate + new Vector2Int((int)dirX, (int)dirY));

        itemMagnet.TransportItem(transform.position, itemSize, dir, targetSlot, availableItems);
        animator.SetTrigger("startRunning");
    }

    private void OnPowerDischarged()
    {
        itemMagnet.SetAvailable(true);
    }

    public override void OnMouseClick()
    {
        transform.localScale = new Vector3(
            transform.localScale.x * (-1),
            transform.localScale.y,
            transform.localScale.z);
    }

    public override void StartFunctions()
    {
        powerChecker.StartPowerCheck();
        itemMagnet.SetAvailable(true);
    }

    public override void StopFunctions()
    {
        powerChecker.StopPowerCheck();
    }
}
