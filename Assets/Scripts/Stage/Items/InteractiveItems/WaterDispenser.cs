using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterDispenser : InteractiveItem
{
    public PowerChecker powerChecker;
    public WaterGenerator waterGenerator;

    private void Start()
    {
        powerChecker.onChargeStart = OnPowerChargeStart;
        powerChecker.onCharged = OnPowerCharged;
        powerChecker.onDischarged = OnPowerDischarged;
    }

    private void OnPowerChargeStart() { }

    private void OnPowerCharged(Vector2Int powerSourcePos)
    {
        Vector3 genPos = transform.position + new Vector3(0, itemSize.y / 2.0f * (-1), 0);

        waterGenerator.GenerateStart(genPos);
        animator.SetBool("isRunning", true);
    }

    private void OnPowerDischarged()
    {
        waterGenerator.GenerateStop();
        animator.SetBool("isRunning", false);
    }

    public override void OnMouseClick() { }

    public override void StartFunctions()
    {
        powerChecker.StartPowerCheck();
    }

    public override void StopFunctions()
    {
        powerChecker.StopPowerCheck();
        waterGenerator.GenerateStop();
        animator.SetBool("isRunning", false);
    }
}
