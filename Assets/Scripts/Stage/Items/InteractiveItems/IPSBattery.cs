using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IPSBattery : InteractiveItem
{
    public PowerChecker powerChecker;
    public PowerGenerator powerGenerator;

    private void Start()
    {
        powerChecker.onChargeStart = OnPowerChargeStart;
        powerChecker.onCharged = OnPowerCharged;
        powerChecker.onDischarged = OnPowerDischarged;
    }

    private void OnPowerChargeStart() { }

    private void OnPowerCharged(Vector2Int powerSourcePos)
    {
        animator.SetTrigger("batteryCharge");
    }

    private void OnPowerDischarged()
    {
        powerGenerator.GenerateStart();
        animator.SetTrigger("batteryOn");
    }

    public override void OnMouseClick() { }

    public override void StartFunctions()
    {
        powerChecker.StartPowerCheck();
    }

    public override void StopFunctions()
    {
        powerChecker.StopPowerCheck();
        powerGenerator.GenerateStop();
        animator.SetTrigger("batteryOff");
    }
}
