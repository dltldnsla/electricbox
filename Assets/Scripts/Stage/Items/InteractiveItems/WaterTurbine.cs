using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterTurbine : InteractiveItem
{
    public WaterChecker waterChecker;
    public PowerGenerator powerGenerator;

    private void Start()
    {
        waterChecker.onCharged = OnWaterCharged;
        waterChecker.onDischarged = OnWaterDischarged;
    }

    private void OnWaterCharged()
    {
        powerGenerator.GenerateStart();
        animator.SetBool("isRunning", true);
    }

    private void OnWaterDischarged()
    {
        powerGenerator.GenerateStop();
        animator.SetBool("isRunning", false);
    }

    public override void OnMouseClick() { }

    public override void StartFunctions()
    {
        waterChecker.StartWaterCheck();
    }

    public override void StopFunctions()
    {
        waterChecker.StopWaterCheck();
        powerGenerator.GenerateStop();
        animator.SetBool("isRunning", false);
    }
}
