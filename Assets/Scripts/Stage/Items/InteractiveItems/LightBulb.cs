using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightBulb : InteractiveItem
{
    public PowerChecker powerChecker;
    public LightGenerator lightGenerator;

    private void Start()
    {
        powerChecker.onChargeStart = OnPowerChargeStart;
        powerChecker.onCharged = OnPowerCharged;
        powerChecker.onDischarged = OnPowerDischarged;
    }

    private void OnPowerChargeStart() { }

    private void OnPowerCharged(Vector2Int powerSourcePos)
    {
        Vector2[] lightDir =
        {
            Vector2.up,
            Vector2.down,
            Vector2.left,
            Vector2.right
        };

        lightGenerator.GenerateStart(transform.position, itemSize, lightDir);
        animator.SetBool("isRunning", true);
    }

    private void OnPowerDischarged()
    {
        lightGenerator.GenerateStop();
        animator.SetBool("isRunning", false);
    }

    public override void OnMouseClick() { }

    public override void StartFunctions()
    {
        powerChecker.StartPowerCheck();
    }

    public override void StopFunctions()
    {
        powerChecker.StopPowerCheck();
        lightGenerator.GenerateStop();
        animator.SetBool("isRunning", false);
    }
}
