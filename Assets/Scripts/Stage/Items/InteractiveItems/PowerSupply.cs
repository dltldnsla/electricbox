using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerSupply : InteractiveItem
{
    public PowerGenerator powerGenerator;

    public override void OnMouseClick()
    {
        PlayManager pm = GameObject.Find("PlayManager").GetComponent<PlayManager>();
        
        if(!pm.isSimulating)
        {
            pm.StartSimulate();
        }
        else
        {
            pm.StopSimulate();
        }
    }

    public override void StartFunctions()
    {
        powerGenerator.GenerateStart();
        animator.SetBool("isRunning", true);
    }

    public override void StopFunctions()
    {
        powerGenerator.GenerateStop();
        animator.SetBool("isRunning", false);
    }
}
