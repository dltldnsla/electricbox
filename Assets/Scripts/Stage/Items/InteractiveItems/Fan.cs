using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Fan : InteractiveItem
{
    public PowerChecker powerChecker;
    public WindGenerator windGenerator;
    public AudioSource fanSound;

    private void Start()
    {
        powerChecker.onChargeStart = OnPowerChargeStart;
        powerChecker.onCharged = OnPowerCharged;
        powerChecker.onDischarged = OnPowerDischarged;
    }

    private void OnPowerChargeStart() { }

    private void OnPowerCharged(Vector2Int powerSourcePos)
    {
        float dirX = transform.localScale.x > 0 ? -1 : 1;
        float dirY = 0;
        Vector2 dir = new Vector2(dirX, dirY);

        windGenerator.GenerateStart(transform.position, itemSize, dir);
        animator.SetBool("isRunning", true);
        fanSound.Play();
    }

    private void OnPowerDischarged()
    {
        windGenerator.GenerateStop();
        animator.SetBool("isRunning", false);
    }

    public override void OnMouseClick()
    {
        transform.localScale = new Vector3(
            transform.localScale.x * (-1),
            transform.localScale.y,
            transform.localScale.z);
    }

    public override void StartFunctions()
    {
        powerChecker.StartPowerCheck();
    }

    public override void StopFunctions()
    {
        powerChecker.StopPowerCheck();
        windGenerator.GenerateStop();
        animator.SetBool("isRunning", false);
    }
}
