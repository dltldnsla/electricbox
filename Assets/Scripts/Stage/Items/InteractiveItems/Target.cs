using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : InteractiveItem
{
    public PowerChecker powerChecker;

    private void Start()
    {
        powerChecker.onChargeStart = OnPowerChargeStart;
        powerChecker.onCharged = OnPowerCharged;
        powerChecker.onDischarged = OnPowerDischarged;
    }

    private void OnPowerChargeStart() { }

    private void OnPowerCharged(Vector2Int powerSourcePos)
    {
        animator.SetBool("isRunning", true);

        PlayManager pm = GameObject.Find("PlayManager").GetComponent<PlayManager>();
        pm.StageClear();
    }

    private void OnPowerDischarged()
    {
        animator.SetBool("isRunning", false);
    }

    public override void OnMouseClick() { }

    public override void StartFunctions()
    {
        powerChecker.StartPowerCheck();
    }

    public override void StopFunctions()
    {
        powerChecker.StopPowerCheck();
    }
}
