using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserDevice : InteractiveItem
{
    public PowerChecker powerChecker;
    public LaserGenerator laserGenerator;

    private void Start()
    {
        powerChecker.onChargeStart = OnPowerChargeStart;
        powerChecker.onCharged = OnPowerCharged;
        powerChecker.onDischarged = OnPowerDischarged;
    }

    private void OnPowerChargeStart() { }

    private void OnPowerCharged(Vector2Int powerSourcePos)
    {
        ItemSlot genSlot = Utility.GetParentItemSlot(this);
        ItemSlotGrid gameZone = Utility.GetParentItemSlotGrid(genSlot);

        int dirX = transform.localScale.x > 0 ? 1 : -1;
        Vector2Int dir = new Vector2Int(dirX, 0);
        genSlot = gameZone.GetItemSlot(genSlot.slotCoordinate + dir);

        laserGenerator.GenerateStart(genSlot, dir);
        //animator.SetBool("isRunning", true);
    }

    private void OnPowerDischarged()
    {
        laserGenerator.GenerateStop();
        //animator.SetBool("isRunning", false);
    }

    public override void OnMouseClick()
    {
        transform.localScale = new Vector3(
            transform.localScale.x * (-1),
            transform.localScale.y,
            transform.localScale.z);
    }

    public override void StartFunctions()
    {
        powerChecker.StartPowerCheck();
    }

    public override void StopFunctions()
    {
        powerChecker.StopPowerCheck();
        laserGenerator.GenerateStop();
        //animator.SetBool("isRunning", false);
    }
}
