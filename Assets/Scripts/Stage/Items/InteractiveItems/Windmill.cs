using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Windmill : InteractiveItem
{
    public WindChecker windChecker;
    public PowerGenerator powerGenerator;

    private void Start()
    {
        windChecker.onCharged = OnWindCharged;
        windChecker.onDischarged = OnWindDischarged;
    }

    private void OnWindCharged()
    {
        powerGenerator.GenerateStart();
        animator.SetBool("isRunning", true);
    }

    private void OnWindDischarged()
    {
        powerGenerator.GenerateStop();
        animator.SetBool("isRunning", false);
    }

    public override void OnMouseClick() { }

    public override void StartFunctions()
    {
        windChecker.StartWindCheck();
    }

    public override void StopFunctions()
    {
        windChecker.StopWindCheck();
        powerGenerator.GenerateStop();
        animator.SetBool("isRunning", false);
    }
}
