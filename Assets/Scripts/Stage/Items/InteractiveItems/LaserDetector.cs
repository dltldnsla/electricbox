using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserDetector : InteractiveItem
{
    public LaserChecker laserChecker;
    public PowerGenerator powerGenerator;

    private void Start()
    {
        laserChecker.onCharged = OnLaserCharged;
        laserChecker.onDischarged = OnLaserDischarged;
    }

    private void OnLaserCharged()
    {
        powerGenerator.GenerateStart();
    }

    private void OnLaserDischarged()
    {
        powerGenerator.GenerateStop();
    }

    public override void OnMouseClick()
    {
        transform.localScale = new Vector3(
            transform.localScale.x * (-1),
            transform.localScale.y,
            transform.localScale.z);
    }

    public override void StartFunctions()
    {
        laserChecker.StartLaserCheck();
    }

    public override void StopFunctions()
    {
        laserChecker.StopLaserCheck();
        powerGenerator.GenerateStop();
    }
}
