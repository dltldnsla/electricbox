using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteamDetector : InteractiveItem
{
    public SteamChecker steamChecker;
    public PowerGenerator powerGenerator;

    private void Start()
    {
        steamChecker.onCharged = OnSteamCharged;
        steamChecker.onDischarged = OnSteamDischarged;
    }

    private void OnSteamCharged()
    {
        powerGenerator.GenerateStart();
        animator.SetBool("isRunning", true);
    }

    private void OnSteamDischarged()
    {
        powerGenerator.GenerateStop();
        animator.SetBool("isRunning", false);
    }

    public override void OnMouseClick() { }

    public override void StartFunctions()
    {
        steamChecker.StartSteamCheck();
    }

    public override void StopFunctions()
    {
        steamChecker.StopSteamCheck();
        powerGenerator.GenerateStop();
        animator.SetBool("isRunning", false);
    }
}
