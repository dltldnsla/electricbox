using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemSlot : MonoBehaviour
{
    public enum Color
    {
        Default, Red, Green
    }

    public InteractiveItem containedItem;
    public List<ConnectionWire> connectedWires;
    public List<WindGenerator> windSources;
    public Vector2Int slotCoordinate;

    private Color32 defaultColor;

    private void Awake()
    {
        defaultColor = GetComponent<SpriteRenderer>().color;
    }

    // 슬롯 색상 변경
    public void SetSlotColor(Color color)
    {
        Color32 color32 = defaultColor;

        switch (color)
        {
            case Color.Default:
                color32 = defaultColor;
                break;
            case Color.Red:
                color32 = new Color32(255, 0, 0, 63);
                break;
            case Color.Green:
                color32 = new Color32(0, 255, 0, 63);
                break;
            default:
                break;
        }

        GetComponent<SpriteRenderer>().color = color32;
    }

    // 현재 슬롯에 작용하는 바람 방향의 합계를 반환
    public Vector2 GetWindDirection()
    {
        Vector2 dir = Vector2.zero;

        foreach (WindGenerator windSource in windSources)
        {
            dir += windSource.GetWindDirection();
        }

        return dir;
    }
}
