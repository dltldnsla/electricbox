using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ItemSlotGrid : MonoBehaviour
{
    public Vector2Int gridSize;
    public float padding;

    public GameObject bgPrefab;
    public GameObject slotPrefab;

    private ItemSlot[,] slotGrid;
    private Vector2 slotSize;

    private void Awake()
    {
        Vector2 slotSprRendSize = slotPrefab.GetComponent<SpriteRenderer>().size;
        Vector2 slotLocalScale = slotPrefab.transform.localScale;
        slotSize = new Vector2(slotSprRendSize.x * slotLocalScale.x, slotSprRendSize.y * slotLocalScale.y);

        SetBackground();
        CreateSlots();
    }

    // 물, 증기 등의 오브젝트가 영역을 벗어날 경우 제거
    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.CompareTag("Particle"))
        {
            Particle particle = collision.GetComponent<Particle>();

            // 레이저인 경우, 속도를 0으로 설정하여 Autodestruct로 제거되도록 함
            if (particle is Laser laser)
            {
                laser.speed = 0;
            }
            else
            {
                Destroy(collision.gameObject);
            }
        }
    }

    // 인벤토리 영역의 배경 크기를 설정
    private void SetBackground()
    {
        float width = gridSize.x * slotSize.x + padding * 2;
        float height = gridSize.y * slotSize.y + padding * 2;

        GameObject bg = Instantiate(bgPrefab);
        bg.transform.SetParent(transform);
        bg.transform.localPosition = new Vector3(width / 2, -height / 2, 0);
        bg.GetComponent<SpriteRenderer>().size = new Vector2(width, height);

        // 인벤토리 영역의 Collider 설정
        BoxCollider2D collider = transform.GetComponent<BoxCollider2D>();
        collider.size = new Vector2(width, height);
        collider.offset = new Vector2(width / 2, -height / 2);
    }

    // 아이템 슬롯 생성 및 배치
    private void CreateSlots()
    {
        slotGrid = new ItemSlot[gridSize.x, gridSize.y];
        GameObject slot = null;

        for (int y = 0; y < gridSize.y; y++)
        {
            for (int x = 0; x < gridSize.x; x++)
            {
                slot = Instantiate(slotPrefab);
                slot.transform.SetParent(transform);
                slot.transform.localPosition = new Vector3((x + 0.5f) * slotSize.x + padding, -((y + 0.5f) * slotSize.y + padding), 0);

                slotGrid[x, y] = slot.GetComponent<ItemSlot>();
                slotGrid[x, y].slotCoordinate = new Vector2Int(x, y);
            }
        }
    }

    // 지정된 좌표의 아이템 슬롯을 반환(잘못된 좌표일 경우 null 반환)
    public ItemSlot GetItemSlot(Vector2Int slotCoordinate)
    {
        ItemSlot slot = null;

        if (slotCoordinate.x >= 0 && slotCoordinate.y >= 0
            && slotCoordinate.x < gridSize.x && slotCoordinate.y < gridSize.y)
        {
            slot = slotGrid[slotCoordinate.x, slotCoordinate.y];
        }

        return slot;
    }

    private Vector2Int GetEndCoordinate(Vector2Int startCoordinate, Item item)
    {
        Vector2Int itemSize = item.itemSize;
        int endCoordinateX = startCoordinate.x + itemSize.x - 1;
        int endCoordinateY = startCoordinate.y + itemSize.y - 1;

        return new Vector2Int(endCoordinateX, endCoordinateY);
    }

    // 아이템 드롭 영역 내의 슬롯 색상을 변경
    public void SetSlotColor(Vector2Int startCoordinate, Item item, ItemSlot.Color slotColor)
    {
        Vector2Int endCoordinate = GetEndCoordinate(startCoordinate, item);

        // 아이템 드롭 영역이 인벤토리 경계를 벗어났을 경우, 색 변경 범위 재설정
        if(CheckItemCrossingBorder(startCoordinate, item))
        {
            endCoordinate.x = endCoordinate.x >= gridSize.x ? gridSize.x - 1 : endCoordinate.x;
            endCoordinate.y = endCoordinate.y >= gridSize.y ? gridSize.y - 1 : endCoordinate.y;
        }

        // 슬롯 색 변경
        for (int x = startCoordinate.x; x <= endCoordinate.x; x++)
        {
            for (int y = startCoordinate.y; y <= endCoordinate.y; y++)
            {
                slotGrid[x, y].SetSlotColor(slotColor);
            }
        }
    }

    // 아이템을 지정된 슬롯에 배치
    public void PlaceItem(Vector2Int startCoordinate, Item item)
    {
        SetItem(startCoordinate, item);
        item.transform.parent = GetItemSlot(startCoordinate).transform;
        item.transform.position = item.transform.parent.position + item.GetFirstSlotPos();
    }

    // 아이템 드롭 영역 내의 슬롯에 아이템 정보 추가
    public void SetItem(Vector2Int startCoordinate, Item item)
    {
        Vector2Int endCoordinate = GetEndCoordinate(startCoordinate, item);

        for (int x = startCoordinate.x; x <= endCoordinate.x; x++)
        {
            for (int y = startCoordinate.y; y <= endCoordinate.y; y++)
            {
                // 아이템의 종류에 따라 아이템 정보를 배치
                if (item is InteractiveItem interactiveItem)
                {
                    slotGrid[x, y].containedItem = interactiveItem;
                }
                else if (item is ConnectionWire connectionWire)
                {
                    slotGrid[x, y].connectedWires.Add(connectionWire);
                }
            }
        }
    }

    // 아이템 이동 시, 아이템이 있던 기존 슬롯의 아이템 정보 제거
    public void RemoveItem(Vector2Int startCoordinate, Item item)
    {
        Vector2Int endCoordinate = GetEndCoordinate(startCoordinate, item);

        for (int x = startCoordinate.x; x <= endCoordinate.x; x++)
        {
            for (int y = startCoordinate.y; y <= endCoordinate.y; y++)
            {
                // 아이템의 종류에 따라 아이템 정보를 제거
                if (item is InteractiveItem)
                {
                    slotGrid[x, y].containedItem = null;
                }
                else if (item is ConnectionWire connectionWire)
                {
                    slotGrid[x, y].connectedWires.Remove(connectionWire);
                }
            }
        }
    }

    // 아이템을 드롭 가능한지 체크
    public bool CheckItemDroppable(Vector2Int startCoordinate, Item item)
    {
        bool result = false;

        if(!CheckItemCrossingBorder(startCoordinate, item))
        {
            if(item is InteractiveItem interactiveItem)
            {
                if(!CheckItemAlreadyOccupied(startCoordinate, interactiveItem))
                {
                    result = true;
                }
            }
            else if(item is ConnectionWire connectionWire)
            {
                if(!CheckWireAlreadyOccupied(startCoordinate, connectionWire))
                {
                    result = true;
                }
            }
        }

        return result;
    }

    // 아이템 드롭 영역이 인벤토리 경계를 벗어났을 경우 true, 아닐 경우 false를 반환
    private bool CheckItemCrossingBorder(Vector2Int startCoordinate, Item item)
    {
        Vector2Int itemSize = item.itemSize;
        bool result = false;

        if (gridSize.x < startCoordinate.x + itemSize.x
            || gridSize.y < startCoordinate.y + itemSize.y) 
        {
            result = true;
        }

        return result;
    }

    // 아이템 드롭 영역 내에 다른 아이템이 있을 경우 true, 아닐 경우 false를 반환 
    private bool CheckItemAlreadyOccupied(Vector2Int startCoordinate, InteractiveItem item)
    {
        Vector2Int endCoordinate = GetEndCoordinate(startCoordinate, item);
        bool result = false;

        for (int x = startCoordinate.x; x <= endCoordinate.x; x++)
        {
            for (int y = startCoordinate.y; y <= endCoordinate.y; y++)
            {
                if (slotGrid[x, y].containedItem != null
                    && slotGrid[x, y].containedItem != item)
                {
                    result = true;
                    break;
                }
            }
        }

        return result;
    }

    // 전선 드롭 위치에 다른 전선이 있을 경우 true, 아닐 경우 false를 반환 
    private bool CheckWireAlreadyOccupied(Vector2Int startCoordinate, ConnectionWire wire)
    {
        Vector2Int endCoordinate = GetEndCoordinate(startCoordinate, wire);
        bool result = false;

        List<ConnectionWire> tempList = slotGrid[startCoordinate.x, startCoordinate.y].connectedWires;

        // 드롭 영역 내의 모든 슬롯에 공통적으로 존재하는 전선 검색
        for (int x = startCoordinate.x; x <= endCoordinate.x; x++)
        {
            for (int y = startCoordinate.y; y <= endCoordinate.y; y++)
            {
                tempList = tempList.Intersect(slotGrid[x, y].connectedWires).ToList();
            }
        }

        // 모든 슬롯에 공통되는 전선이 있을 경우 true 반환
        if(tempList.Count > 0)
        {
            if(tempList[0] != wire)
            {
                result = true;
            }
        }

        return result;
    }

    //// 지정한 범위 내에서 전원 공급 장치를 검색
    //public Item FindPowerSource(Vector2Int startCoordinate, Item item, out Vector2Int powerSourcePos)
    //{
    //    Item result = null;
    //    Vector2Int endCoordinate = GetEndCoordinate(startCoordinate, item);

    //    for (int x = startCoordinate.x; x <= endCoordinate.x; x++)
    //    {
    //        for (int y = startCoordinate.y; y <= endCoordinate.y; y++)
    //        {
    //            result = slotGrid[x, y].FindPowerSource(item);

    //            if (result != null)
    //            {
    //                powerSourcePos = new Vector2Int(x, y);
    //                return result;
    //            }
    //        }
    //    }

    //    powerSourcePos = new Vector2Int(-1, -1);
    //    return result;
    //}
}
