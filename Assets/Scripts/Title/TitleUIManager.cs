using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleUIManager : MonoBehaviour
{
    private void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
        // 데이터 초기화
        GameManager gm = GameManager.GetInstance();
        gm.ResetData();
    }

    public void GoToLevels()
    {
        SceneManager.LoadScene("Levels");
    }
}
