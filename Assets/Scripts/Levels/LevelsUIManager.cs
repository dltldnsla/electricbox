using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelsUIManager : MonoBehaviour
{
    public GameObject levelListPnl;
    public StageBtn StageBtn;

    private int reachedStage;

    private void Start()
    {   reachedStage = PlayerPrefs.GetInt("ReachedStage");

        GameManager gm = GameManager.GetInstance();
        gm.ResetData();

        for (int i = 1; i <= GameManager.LastStage; i++)
        {
            StageBtn btn = Instantiate(StageBtn);
            bool isUnlocked = (i <= reachedStage + 1);

            btn.SetStageInfo(i, isUnlocked);
            btn.transform.SetParent(levelListPnl.transform);
        }
    }

    public void GoToTitle()
    {
        SceneManager.LoadScene("Title");
    }
}
