using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class StageBtn : MonoBehaviour
{
    public TMP_Text stageNumTxt;
    public Sprite blockedSprite;

    public int stageNum;
    public bool stageUnlocked;

    public void SetStageInfo(int num, bool unlocked)
    {
        stageNum = num;
        stageUnlocked = unlocked;

        if (stageUnlocked)
        {
            stageNumTxt.text = string.Format("{0:D2}", stageNum);
        }
        else
        {
            Image image = GetComponent<Image>();
            image.sprite = blockedSprite;

            stageNumTxt.gameObject.SetActive(false);
        }
    }

    public void GoToStage()
    {
        if (stageUnlocked)
        {
            GameManager gm = GameManager.GetInstance();
            gm.currentStage = stageNum;

            SceneManager.LoadScene("Stage");
        }
    }
}
