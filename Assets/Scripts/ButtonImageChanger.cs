using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonImageChanger : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Image image;

    public Sprite defaultSprite;
    public Sprite mouseOverSprite;

    public void OnPointerEnter(PointerEventData eventData)
    {
        image.sprite = mouseOverSprite;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        image.sprite = defaultSprite;
    }
}
